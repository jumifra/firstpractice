<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <?php part('header-tags'); ?>
    <?php wp_head(); ?>
    <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="<?php echo TEMPLATE_URL; ?>/respond.js"></script>
    <![endif]-->
</head>

<body>