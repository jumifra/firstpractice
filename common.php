<?php

// Traducciones
load_theme_textdomain( LANG_DOMAIN, dirname(__FILE__) . '/lang/');

// PicolCodeSuite
require_once 'pcs/pcs.php';


// Setup Theme

function picol_theme_setup(){

    PicolUpdater::theme(__FILE__, 'http://dev.picolestudio.pe/api/');

    $main_js_file = 'concat' . (!WP_DEBUG ? '.min' : '') . '.js';
    PicolScripts::registerScript('main-scripts', TEMPLATE_URL . '/' . $main_js_file, array(), true );
    PicolScripts::registerStyle('main-style', TEMPLATE_URL . '/style.css' );

}

function forceHTTPS(){
    if( !WP_DEBUG && (empty($_SERVER["HTTPS"]) || $_SERVER["HTTPS"] !== "on")) {
        header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"], true, 301);
        exit();
    }
}


function part( $name, $suffix = ''){
    get_template_part('parts/' . $name, $suffix);
}

function _t( $text ) {
    return __($text, LANG_DOMAIN);
}

function _et( $text ) {
    echo _t( $text );
}

function _xt( $text, $context = '' ) {
    return _x( $text, $context, LANG_DOMAIN );
}

function getCurrentLanguage(){
    if(function_exists('qtrans_getLanguage')){
        return qtrans_getLanguage();
    }
    return false;
}

// Shorts de get_field, y get_post_meta

function safe_get_field( $field_name, $fallback = false, $post_id = null ){
    $value = get_field( $field_name, $post_id );

    if(!$value){
        $value = $fallback;
    }

    return $value;
}

function safe_get_post_meta( $post_id, $meta_name, $fallback = false ){
    $value = get_post_meta( $post_id, $meta_name , true);

    if(!$value){
        $value = $fallback;
    }

    return $value;
}

// Menus

function quick_nav_menu( $menu_location, $ul_class = ''){
    wp_nav_menu(array(
        'theme_location' => $menu_location,
        'container' => 'false',
        'items_wrap' => '<ul class="'.$ul_class.'">%3$s</ul>'
    ));
}

function get_menu_items( $menu_name ) {
    if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
        $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
        $menu_items = wp_get_nav_menu_items($menu->term_id);
        return (array) $menu_items;
    }
    return false;
}

/*
 * Añadir editor para edición de menús
 */

// get the the role object
$role_object = get_role( 'editor' );
// add $cap capability to this role object
$role_object->add_cap( 'edit_theme_options' );

function fit_image_tag( $image_id, $size = 'large', $class = '', $args = array() ){
    $image_attrs = array(
        'class' => $class . ' | js-fit-image'
    );
    $image_attrs = array_merge( $image_attrs, $args );
    echo wp_get_attachment_image( $image_id, $size, false, $image_attrs);
}