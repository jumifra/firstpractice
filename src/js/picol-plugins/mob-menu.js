jQuery( function($) {

    $('.js-mob-menu').each(function(i, el){

        var $el = $(el),
            button_sel = $el.data('button'),
            $mob_btn = $(button_sel),
            $mob_menu = $el,
            $lis = $mob_menu.find('li'),
            $menu_items = $mob_menu.find('li a'),
            $sub_items = $mob_menu.find('li ul'),
            menu_abierto = false;

        function cerrarMenu(){
            $mob_menu.slideUp();
            menu_abierto = false
        }

        function abrirMenu(e){
            $sub_items.hide();
            $lis.removeClass('open');
            $mob_menu.slideDown();
            if(!menu_abierto){
                e.stopPropagation();
            }
            menu_abierto = true;

        }

        $menu_items.on('click', function(e){
            var $parent = $(this).parent();
            e.stopPropagation();
            if($(this).siblings('ul').length){

                if($parent.hasClass('open')){
                    $parent.removeClass('open');
                    $(this).siblings('ul').slideUp();
                } else {
                    $(this).siblings('ul').slideDown();
                    $parent.addClass('open');
                }

                e.preventDefault();
            }
        });

        $mob_btn.on('click', function(e){
            abrirMenu(e);
            e.preventDefault();
        });
        $('body').on('click', cerrarMenu);
        $(window).on('resize', function(){
            if( menu_abierto && !$mob_btn.is(':visible') ) {
                $mob_menu.hide();
                menu_abierto = false;
            }
        });

    });



});