jQuery(function($){
    function prepareFancySelect( $jqueryObj ){

        $jqueryObj = $jqueryObj || $('select.fancy-select');

        $jqueryObj.each(function(i,el){
            var $el = $(el),
                klass = $el.data('class'),
                $wrap = $('<div class="'+klass+'"></div>'),
                $span = $('<span></span>');

            $el.replaceWith($wrap);
            $wrap.append($el);
            $wrap.append($span);
            $el.removeClass('fancy-select');

            function updateValue(){
                $span.html( $el.find('option:selected').html() );
            }

            $el.on( 'change', updateValue );

            $el.on('focus', function(){
                $wrap.addClass('focus');
            });

            $el.on('blur', function(){
                $wrap.removeClass('focus');
            });

            updateValue();

        });
    }

    prepareFancySelect();

    window.fancySelect = prepareFancySelect;
});