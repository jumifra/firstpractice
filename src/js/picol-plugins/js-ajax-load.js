jQuery(function($){

    $('body').on('click', '.js-ajax-load', function(e){
        var $this = $(this),
            target_sel = $this.data('target'),
            url = $this.attr('href'),
            callback_name = $this.data('callback');

        $this.attr('disabled', 'disabled');

        $this.html('<i class="fa fa-spin fa-spinner"></i>');

        $.get(
            url,
            {},
            function(data){
                var $newHtml = $(data),
                    $content = $($newHtml.find(target_sel).html());

                $content.css('opacity', 0);
                $this.replaceWith($content);

                if(callback_name && window[callback_name]){
                    window[callback_name]( $content );
                }

                $content.animate({
                    opacity : 1
                }, 500);
            }
        );

        e.preventDefault();
    });

});