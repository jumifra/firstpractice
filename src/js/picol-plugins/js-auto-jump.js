jQuery(function($){
    $('body').on('change', 'select.js-auto-jump', function(){
        window.location = $(this).val();
        $(this).attr('disabled', 'disabled');
        $(this).parent().addClass('disabled');
    });
});