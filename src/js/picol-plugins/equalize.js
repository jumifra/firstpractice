jQuery(function ($) {
    var $w = $(window);
    $.fn.equalize = function (options) {

        var that = this;
        options = options || {};

        function onResize(){
            var clock = getClock();
            process(clock);
        }

        function process(clock){

            var max, group;

            that.css('height', 'auto');

            if(!clock){
                return;
            }

            that.each(function(i,el){
                if( i % clock === 0) {

                    if( max && group ){
                        equalize( group, max );
                    }

                    max = 0;
                    group = [];
                }

                max = Math.max(
                    max,
                    $(el).height()
                );

                group.push(el);
            });

            if (group && group.length) {
                equalize( group, max );
            }
        }

        function equalize( group, height ) {
            for( var i = 0, l = group.length; i < l; i ++ ) {
                $(group[i]).css('height', height);
            }
        }

        function getClock(){
            var clock = false,
                width = $w.width();

            for(var i in options) {
                if( options.hasOwnProperty( i )) {
                    if ( width <= i ) {
                        clock = options[i];
                        break;
                    }
                }
            }
            return clock;
        }

        $w.on('resize', onResize );
        onResize();

    }

});