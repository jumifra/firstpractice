(function($){

    $.fn.jsSlider = function( callback, elements ) {

        var $this = $(this),
            fade_time = 200,
            slide_time = 200,
            selectors = {},
            selector_list = ['left', 'right', 'controls', 'slides'],

            beforeTransition = function(){};


        selectors.left = '.js-slider__left';
        selectors.right = '.js-slider__right';
        selectors.controls = '.js-slider__control';
        selectors.slides = '.js-slider__slide';


        if (typeof callback == 'object') {

            if(callback.selectors){
                for( var i = 0; i < selector_list.length; i ++) {
                    if(callback.selectors[ selector_list[i] ]){
                        selectors[selector_list[i]] = callback.selectors[selector_list[i]];
                    }
                }
            }

            if(typeof callback.before == 'function') {
                beforeTransition = callback.before;
            } else{
                beforeTransition = function(){};
            }


            if(typeof callback.after == 'function'){
                callback = callback.after;
            } else {
                callback = function(){};
            }


        } else if(typeof callback == 'string'){

            if( this[callback] ){
                this[callback](elements);
            }

        } else {
            callback = typeof callback == 'undefined' ? function(){} : callback;
        }

        $this.each(function(i,el){

            var $slider = $(el),
                $slides = $slider.find(selectors.slides),
                $controls = $slider.find(selectors.controls),
                $left = $slider.find(selectors.left),
                $right = $slider.find(selectors.right),
                cycling = $slider.data('cycling') || false,
                trigger_resize = $slider.data('trigger-resize') || false,
                delay = $slider.data('delay'),
                method = $slider.data('method') || 'fade',
                fading_time = $slider.data('fade-time') || fade_time,
                sliding_time = $slider.data('slide-time') || slide_time,
                combined_time = fade_time + slide_time,

                controlOn = $slider.data('control-on-class') || 'current',

                length = $slides.length,
                moving = false,
                the_interval,

                current_i;

            delay = delay ? delay : 7000;

            function goTo(i){

                i = i % length;

                if( moving || i == current_i ) {
                    return;
                }

                if(the_interval){
                    clearInterval(the_interval);
                    the_interval = setInterval(next, delay);
                }
                swapSlides( i );
                markControl( i );

            }

            function swapSlides( i ){
                var finish_cb,
                    $current_slide, $next_slide;

                finish_cb = function(){

                    moving = false;
                    current_i = i;
                    callback(i,$next_slide.eq(0));
                    if( trigger_resize ){
                        $(window).trigger('resize');
                    }

                };

                $next_slide = $slides.eq(i);
                if( typeof current_i == 'undefined' ) {
                    $next_slide.fadeIn(combined_time, finish_cb);
                } else {
                    $current_slide = $slides.eq(current_i);

                    if( method == 'slideReplace' ) {

                        $current_slide.slideReplace($next_slide, {
                            slideSpeed : sliding_time,
                            fadeSpeed : fading_time,
                            callback : finish_cb
                        });
                    } else {
                        $current_slide.fadeOut( fading_time );
                        $next_slide.fadeIn( fading_time, finish_cb );
                    }
                }

                beforeTransition( i, $next_slide );
            }

            function markControl(i){
                $controls
                    .removeClass(controlOn)
                    .eq(i)
                    .addClass(controlOn);
            }

            function next(e){
                var n = typeof current_i == 'undefined' ? 1 : current_i + 1;
                goTo( n );
                e && e.preventDefault();
            }

            function prev(e){
                var p = typeof current_i == 'undefined' ? -1 : current_i - 1;
                goTo( p );
                e && e.preventDefault();
            }

            $left.click( prev );
            $right.click( next );

            $controls.each( function(i, el){
                $(el).click(function(e){
                    e.preventDefault();
                    goTo(i);
                })
            });

            if(cycling){
                the_interval = setInterval( next, delay );
            }

            $slides.hide();
            goTo(0);

            if($slides.length < 2){
                $controls.hide();
                $left.hide();
                $right.hide();
            } else {
                $controls.show();
                $left.show();
                $right.show();
            }

            $this.on('click', '[data-goto]', function(e){
                var slide = $(this).data('goto');
                goTo(slide);
            });

        });

    };

}(jQuery));

jQuery( function($){
    $('.js-slider').jsSlider();
});