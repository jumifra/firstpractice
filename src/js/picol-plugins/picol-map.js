jQuery(function($){
    // Map on contact page
    $('.picolMap').each(function(i,el){
        var $el = $(el);
        var lat = $el.data('lat');
        var lng = $el.data('lng');
        var icon = $el.data('icon');
        var zoom = $el.data('zoom') || 17;
        var map_id = 'picol-map-' + i;

        $el.attr('id', map_id);

        var map = new GMaps({
            div : '#' + map_id,
            lat : lat,
            lng : lng,
            scrollwheel: false,
            zoom : zoom
        });

        map.addMarker({
            lat : lat,
            lng : lng,
            icon : icon
        });
    });

});