jQuery( function($){

    $('body').on('click', '[data-ajax-replace]', function(e){

        var $this = $(this),
            target_sel = $this.data('ajax-replace'),
            content_sel = $this.data('contents'),
            parent_sel = $this.data('parent') || '*',
            url = $this.data('url') || $this.attr('href'),
            $target = $(target_sel),
            $parent = $this.parent( parent_sel );

        $this
            .html('Cargando... ')
            .attr('disabled', 'disabled');


        $parent.addClass("is-ajax-loading");

        $.get(
            url,
            {},
            function(data){
                var $new_doc = $(' ' + data),
                    new_elems = $new_doc.find(content_sel).html(),
                    $new_elems = $(new_elems),
                    $fit_img = $new_elems.find('.js-fit-image');

                $parent.removeClass("is-ajax-loading");

                $target.replaceWith($new_elems);
                fitImagePrepare( $fit_img );
                $new_elems.find('.ps-gallery').each(window.ps_gallery);

            }
        );

        e.returnValue = -1;
        e.preventDefault();
    });

});