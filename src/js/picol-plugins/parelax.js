// RequestAnimationFrame shim

window.rAF = (function(){
    return window.requestAnimationFrame       ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame    ||
        window.oRequestAnimationFrame      ||
        window.msRequestAnimationFrame     ||
        function( callback ){
            window.setTimeout(callback, 1000 / 60);
        };
}());


jQuery(function($){

    var win = window,

        last_scroll_y = 0,
        win_height = 0,

        containers = [],

        ticking = false;

    // Event functions

    function onResize() {
        win_height = parseInt( $(win).height() , 10);
        updateElements();
    }

    function onScroll () {
        if(!ticking){
            ticking = true;
            last_scroll_y = win.scrollY;
            rAF( updateElements );
        }
    }

    // Preparar containers

    function setupContainers() {
        $('.parelaxed').each(function(i,el){
            var container,
                elements = [],
                offsetTop = $(el).offset().top,
                height = $(el).outerHeight(),
                end = offsetTop + height,
                center = offsetTop + (height) / 2;

            $(el).find('[data-parelax-ratio]').each(function(i,el){
                el.parelaxRatio = $(el).data('parelax-ratio');
                el.parelaxOrV = parseInt( $(el).css('top') );
                if(isNaN(el.parelaxOrV)){ el.parelaxOrV = 0; }
                elements.push(el);
            });

            container = {
                element : el,
                start : offsetTop,
                height : height,
                end : end,
                center : center,
                elements : elements
            };

            containers.push(container);
        });
    }

    setTimeout(setupContainers, 50);

    // Preparar Elementos

    onResize();

    // Funciones de repaint

    function updateElements() {
        var visibles = visibleContainers();
        ticking = false;
        updateContainers( visibles );
    }

    // Obtener solo los visibles en este momento

    function visibleContainers() {
        var length = containers.length,
            visible_containers = [],
            is_visible,
            win_end = win_height + last_scroll_y;

        for (var i = 0; i < length; i = i + 1){

            is_visible = !(containers[i].start > win_end || containers[i].end < last_scroll_y);

            if( is_visible){
                visible_containers.push(containers[i]);
            }
        }

        return visible_containers;
    }

    function updateContainers( containers ){
        var length = containers.length,
            win_center = last_scroll_y + (win_height / 2) ;



        for( var i = 0; i < length; i ++ ){
            updateOneContainer( containers[i], win_center );
        }
    }

    function updateOneContainer( container, win_center ){
        var elements = container.elements,
            length = elements.length,
            dif = container.center - win_center;

        for (var i = 0; i < length; i ++) {
            updateOneElement(elements[i], dif);
        }
    }

    function updateOneElement(element, distance) {
        var top = (element.parelaxOrV + distance * element.parelaxRatio) + 'px';

        element.style.top = top;
    }

    win.addEventListener( 'resize', onResize, false);
    win.addEventListener( 'scroll', onScroll, false);

});