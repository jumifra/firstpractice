// Search form filter

(function(){
    $('.js-sanitized').on('submit',function(){
        var  $input = $(this).find('input[type=text], textarea');

        $input.each(function(i,el){

            var $el = $(el),
                val = $el.val();

            val = val.replace(/(<([^>]+)>)/ig,"");
            val = val.replace('(',"");
            val = val.replace(')',"");
            val = val.replace('<',"");
            val = val.replace('>',"");

            $el.val( val );
        });
    });
}());