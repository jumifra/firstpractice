jQuery(function($){

    window.ps_gallery = function(i,el){
        var $el = $(el),
            $reel = $el.find('.ps-gallery__reel'),
            $items = $el.find('.ps-gallery__thumb-ctrl'),
            $left = $el.find('.ps-gallery__prev-arrow--left'),
            $right = $el.find('.ps-gallery__prev-arrow--right'),
            max_pos = $items.length - 4,
            items_width,
            current_prev_pos = 0;

        $el.jsSlider({

            before: function(i){
                makeVisible(i)
            }

        });

        function makeVisible( i ) {
            var at_least = i - 3,
                at_most = i;

            if( current_prev_pos < at_least) {
                goTo( at_least );
            } else if( current_prev_pos > at_most ) {
                goTo( at_most );
            }

        }

        function goTo( i ) {
            i = Math.max(0, Math.min(i, max_pos));
            var destination = -i * items_width;
            $reel.stop().animate({
                left: destination
            });

            current_prev_pos = i;

        }

        function recalc(){
            items_width = $items.eq(0).outerWidth();

            goTo( current_prev_pos );
        }

        recalc();

        $(window).on('resize', recalc);
        $left.on('click', function(){
            goTo( current_prev_pos - 3);
        });
        $right.on('click', function(){
            goTo( current_prev_pos + 3);
        });

    };

    $('.ps-gallery').each(ps_gallery);

});