<?php

// Definir constantes de idioma
define('LANG_DOMAIN', 'nombre-tema');

// Imagen tamaño huge
define('SIZE_HUGE', 'huge');
add_image_size( SIZE_HUGE, 1600, 0);

require_once 'common.php';