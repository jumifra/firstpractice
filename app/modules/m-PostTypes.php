<?php

class PostTypes{

    // const NAME_NOTICIAS = 'noticia';
    // const SLUG_NOTICIAS = 'noticias';

    static function init(){

        /*
        $noticias = new PicolPostType( self::NAME_NOTICIAS, self::SLUG_NOTICIAS );
        $noticias->autoLabels('Noticia', 'Noticias', false);
        */


    }

    static function queryPosts( $post_type, $args = array(), $query = true ){
        $def_args = array(
            'post_type' => $post_type,
            'posts_per_page' => -1,
            'numberposts' => -1
        );

        $args = array_merge( $def_args, $args );

        if ($query) {
            return new WP_Query( $args );
        } else {
            return get_posts( $args );
        }
    }

}
