exports.vendor = {
    jquery : 'src/js/vendor/jquery-1.11.3.min.js',
    appendAround : 'src/js/vendor/append-around.js',
    gmaps : 'src/js/vendor/gmaps.js',
    matchHeight : 'src/js/vendor/jquery.matchheight.js',
    jqueryValidate : 'src/js/vendor/jquery.validate.min.js',
    jsCookie : 'src/js/vendor/js.cookie.min.js',
    slick : 'src/js/vendor/slick.min.js',
    stickyKit : 'src/js/vendor/sticky-kit.min.js',
    wow : 'src/js/vendor/wow.min.js'
};

exports.picol = {
    acfMap : 'src/js/picol-plugins/acf-map.js',
    ajaxReplace : 'src/js/picol-plugins/ajax-replace.js',
    equalize : 'src/js/picol-plugins/equalize.js',
    fancySelect : 'src/js/picol-plugins/fancy-select.js',
    fitImage : 'src/js/picol-plugins/fit-image.js',
    ajaxLoad : 'src/js/picol-plugins/js-ajax-load.js',
    autoJump : 'src/js/picol-plugins/js-auto-jump.js',
    jsSlider : 'src/js/picol-plugins/js-slider.js',
    mobMenu : 'src/js/picol-plugins/mob-menu.js',
    niceGallery : 'src/js/picol-plugins/nice-gallery.js',
    parelax : 'src/js/picol-plugins/parelax.js',
    picolMap : 'src/js/picol-plugins/picol-map.js',
    popupoverlay : 'src/js/picol-plugins/popupoverlay.js',
    searchSanitizer : 'src/js/picol-plugins/search-sanitizer.js',
    slideReplace : 'src/js/picol-plugins/slide-replace.js'
};

