<?php

class PicolScripts{

  protected static $scripts;
  protected static $styles;
  protected static $is_setup;


  protected static function setup(){
    if(!isset(self::$is_setup)){

      add_action('wp_enqueue_scripts', array('PicolScripts', '_enqueueScripts'));

      self::$is_setup = true;
      self::$styles = array();
      self::$scripts = array();
    }
  }

  static function registerScript( $name, $file, $deps = array(), $in_footer = false ){
    self::registerInVar( self::$scripts, $name, $file, $deps, $in_footer );
  }

  static function registerStyle( $name, $file, $deps = array(), $in_footer = false ){
    self::registerInVar( self::$styles, $name, $file, $deps, $in_footer );
  }

  protected static function registerInVar( &$var, $name, $file, $deps = array(), $in_footer = false ) {

    self::setup();

    $var[ $name ] = array(
      'src' => $file,
      'deps' => $deps,
      'in_footer' => $in_footer
    );
  }

  static function _enqueueScripts(){

    $ver = PicolTheme::getThemeVersion();

    foreach( self::$scripts as $handle => $data ) {
      wp_enqueue_script( $handle, $data['src'], $data['deps'], $ver, $data['in_footer']);
    }

    foreach( self::$styles as $handle => $data ) {
      wp_enqueue_style( $handle, $data['src'], $data['deps'], $ver );
    }

  }

}

