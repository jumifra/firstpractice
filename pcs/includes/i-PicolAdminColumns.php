<?php

defined('ABSPATH') or die("No script kiddies please!");

class PicolAdminColumns{

    protected $_post_types;
    protected $_columns = array();
    protected $_sortables = array();

    function __construct( $post_types ){

        $post_types = array( $post_types );

        foreach( $post_types as $post_type ) {
            add_action('manage_edit-' . $post_type . '_columns', array( &$this, '_registerAdminColumns'));
            add_action('manage_' . $post_type . '_posts_custom_column', array( &$this, '_customColumn'), 10, 2);
            add_filter('manage_edit-'.$post_type.'_sortable_columns', array(&$this, '_sortableColumns'), 10, 1);
            add_action( 'pre_get_posts', array(&$this, '_modifyQuerySort') );
        }

        $this->_post_types = $post_types;

    }

    final function addColumn($name, $function, $width = null, $class = null, $after = null){

        $code = sanitize_title( $name );

        $this->_columns[ $code ] = array(
            'name'     => $name,
            'function' => $function,
            'width'    => $width,
            'after'    => $after,
            'class'    => $class
        );

        return $this;
    }

    final function setSortable( $name, $meta_key, $numeric = FALSE ){
        $col_name = sanitize_title( $name );
        $this->_sortables[$col_name] = array(
            'meta_key' => $meta_key,
            'numeric' => $numeric
        );
    }

    final function _registerAdminColumns( $cols ){

        $new_cols = array();
        $columns = $this->_columns;

        foreach( $cols as $or_col_code => $or_col){

            $new_cols[ $or_col_code ] = $or_col;

            foreach($columns as $new_col_code => $new_col_data){

                $after = $new_col_data['after'];
                $name  = $new_col_data['name'];

                if(is_null($after)){
                    $after = 'title';
                }

                if( $after == $or_col_code ) {
                    $new_cols[ $new_col_code ] = $name;

                    unset($columns[$new_col_code]);
                }

            }
        }

        return $new_cols;

    }

    final function _customColumn( $col_code, $post_id ) {
        if(isset($this->_columns[$col_code])){

            $to_call = $this->_columns[$col_code]['function'];

            $argument = $post_id;
            if($this->_columns[$col_code]['class']){
                $class = $this->_columns[$col_code]['class'];
                $argument = new $class($post_id);
            }

            if( is_callable( $to_call )) {
                call_user_func( $to_call, $argument );
            }

        }
    }

    final function _sortableColumns( $cols ){
        foreach($this->_sortables as $column => $meta_data) {
            $cols[$column] = $meta_data['meta_key'];
        }

        return $cols;
    }

    final function _modifyQuerySort($query){
        global $pagenow;
        if( ! is_admin() || $pagenow != 'edit.php')
            return;

        $orderby = $query->get( 'orderby');

        if( isset($this->_sortables[ $orderby ]) ) {
            $orderby = $this->_sortables[ $orderby ]['numeric'] ? 'meta_value_num' : 'meta_value';

            $query->set('meta_key',$this->_sortables['meta_key']);
            $query->set('orderby',$orderby);
        }
    }

}