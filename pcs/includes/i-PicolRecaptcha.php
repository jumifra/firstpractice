<?php

class PicolRecaptcha{

    protected static $sitekey;
    protected static $secret;

    protected static $has_script;

    static function setup( $sitekey, $secret ) {
        self::$sitekey = $sitekey;
        self::$secret = $secret;
    }

    static function printHeadScript( $callback = null ){
        $callback_param = '';
        $async = '';

        if(!empty($callback)){
            $callback_param = sprintf('?onload=%s&render=explicit', $callback);
            $async = 'async defer';
        }

        printf( '<script src="https://www.google.com/recaptcha/api.js%s" %s></script>', $callback_param, $async );
        self::$has_script = true;
    }

    static function isValidCaptchaRequest( $recaptcha_secret = null ){

        $recaptcha_secret = is_null($recaptcha_secret) ? self::$secret : $recaptcha_secret;

        if(is_null($recaptcha_secret)) {
            throw new Exception('No se ha definido el valor de Secret de Recaptcha');
        }

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif ( !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        $secret = $recaptcha_secret;
        $recaptcha = _p('g-recaptcha-response');
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$recaptcha);
        $responseData = json_decode($verifyResponse);

        return $responseData->success;

    }

    static function printRecaptchaElement( $sitekey = null ) {
        $sitekey = is_null( $sitekey ) ? self::$sitekey : $sitekey;

        if(is_null($sitekey)) {
            throw new Exception('No se ha definido el valor de Sitekey de Recaptcha');
        }

        printf('<div id="g-recaptcha" class="g-recaptcha" data-sitekey="%s"></div>', $sitekey);
    }
}
