<?php

/**
 * Class PSSuperior
 * Encargada de obtener el post superior para un $post dado.
 */

class PicolSuperior{

    static $post_type_to_pages;

    static function start(){
        add_filter('picol_superior_page', array('PicolSuperior', '_getSuperiorPage'), 10, 2);
        self::$post_type_to_pages = array();
    }

    static function getSuperior( $the_post = null, $apply_filters = TRUE ){

        global $post;
        $superior = FALSE;

        if(is_null($the_post)){
            $the_post = $post;
        }
        if( $the_post instanceof WP_Post){

            if($the_post->post_parent){
                $superior = get_post($the_post->post_parent);
            }
        }

        if(!$superior && $apply_filters){
            $superior = apply_filters('picol_superior_page', $superior, $the_post);
        }

        return $superior;

    }

    /**
     * Devuelve el $post ancestro de un $post determinado.
     *
     * Si $apply filters es verdadero, buscará más allá del ancestro del mismo post_type
     * según los filtros que se hayan configurado en el tema
     *
     * Por ejemplo, el ancestro de un $post de post_type = 'noticia' podría ser la página 'Noticias y Eventos'
     *
     * @param null $the_post
     * @param bool $apply_filters
     * @return bool|mixed|null|void|WP_Post
     */

    static function getAncestor($the_post = null, $apply_filters = TRUE){
        global $post;
        if(is_null($the_post)){
            $the_post = $post;
        }

        $ancestro = $the_post;

        while($superior = self::getSuperior($ancestro, $apply_filters)){
            $ancestro = $superior;
        }

        return $ancestro;
    }

    static function setPageParentForPostType( $post_type, $page_path ) {
        self::$post_type_to_pages[$post_type] = $page_path;
    }

    static function _getSuperiorPage( $superior, $post ) {

        if(in_array($post->post_type, array_keys(self::$post_type_to_pages))){
            $page = get_page_by_path( self::$post_type_to_pages[$post->post_type]);

            if($page){
                $superior = $page;
            }
        }

        return $superior;
    }

    /**
     * Altera el ID actual
     */

    static function hijackCurrentID(){
        global $wp_query, $id_actual_original, $post;
        if( is_singular() ) {
            $id_actual_original = $wp_query->queried_object_id;
            $ancestro = self::getAncestor();
            $id_actual = $ancestro->ID;

            $wp_query->queried_object_id = $id_actual;
        }
    }


    static function restoreCurrentID(){
        global $wp_query, $id_actual_original;

        if( is_singular() ) {
            $wp_query->queried_object_id = $id_actual_original;
        }
    }

}

PicolSuperior::start();