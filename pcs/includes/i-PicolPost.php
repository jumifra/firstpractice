<?php
/**
 * Created by PhpStorm.
 * User: rodrigomurillo
 * Date: 10/31/14
 * Time: 12:01 AM
 */

defined('ABSPATH') or die("No script kiddies please!");

/**
 * Class PicolPost
 * @property $ID
 * @property $post_author
 * @property $post_date
 * @property $post_date_gmt
 * @property $post_content
 * @property $post_title
 * @property $post_excerpt
 * @property $post_status
 * @property $comment_status
 * @property $ping_status
 * @property $post_password
 * @property $post_name
 * @property $to_ping
 * @property $pinged
 * @property $post_modified
 * @property $post_modified_gmt
 * @property $post_content_filtered
 * @property $post_parent
 * @property $guid
 * @property $menu_order
 * @property $post_type
 * @property $post_mime_type
 * @property $comment_count
 * @property $filter
 *
 * @property $thumbnail_id
 */
abstract class PicolPost
{

  const THUMBNAIL_ID = '_thumbnail_id';

  public $ID;
  protected $_picol_object;
  protected $_picol_mode;

  /*
   * Read mode
   */

  protected $_edited_fields = array();

  /*
   * Cache
   */

  protected $_cached_fields = array();

  /*
   * Object Map
   */

  protected static $object_map = array();


  /*
   * Static
   */

  final static protected function getConstantName($name)
  {
    $name          = strtoupper($name);
    $constant_name = 'static::' . $name;

    return $constant_name;
  }

  /*
   * Instance
   */

  final function __construct($id_or_object = null, $mode = 'r')
  {

    $id_or_object = is_null($id_or_object) ? get_the_ID() : $id_or_object;

    if ($id_or_object instanceof WP_Post || $id_or_object instanceof PicolPost) {
      $this->ID            = $id_or_object->ID;
      $this->_picol_object = $id_or_object;
    } else {
      $this->ID = $id_or_object;
    }

    $mode = strtolower($mode);
    if ($mode == 'w') {
      $this->_picol_mode = $mode;
    }

    register_shutdown_function(array(&$this, '_shutDownCleaning'));

  }

  function __get($name)
  {

    $constant_name = PicolPost::getConstantName($name);

    if (defined($constant_name)) {

      $field_name = constant($constant_name);

      return $this->prepareCacheField($field_name);

    } else if (!is_object($this->_picol_object)) {

      $this->loadObject();

    }

    if (isset($this->_picol_object->{$name})) {
      return $this->_picol_object->{$name};
    }

  }

  function __set($name, $value)
  {

    $constant_name = PicolPost::getConstantName($name);

    if (defined($constant_name)) {
      $field_name = constant($constant_name);

      if ($this->_picol_mode == 'w') {

        if (isset($this->_cached_fields[$field_name])) {
          unset($this->_cached_fields[$field_name]);
        }

        $this->update_post_meta($field_name, $value);
        $this->_cached_fields[$field_name] = $value;

      } else {

        $this->_edited_fields[$field_name] = $value;

      }

    }
  }

  function __call($name, $args)
  {

    if (substr($name, 0, 2) == 'is') {
      $rest          = substr($name, 2);
      $constant_name = PicolPost::getConstantName($rest);

      if (defined($constant_name)) {
        $field_name = constant($constant_name);
        $field      = $this->prepareCacheField($field_name);

        return $field == 1;
      }
    }

  }

  final protected function prepareCacheField($field_name)
  {
    if (!isset($this->_cached_fields[$field_name])) {
      $value = function_exists('get_field') ?
        $this->get_field($field_name) :
        $this->get_post_meta($field_name);

      $this->_cached_fields[$field_name] = $value;
    }

    return $this->_cached_fields[$field_name];
  }

  final function get_field($field_name)
  {
    return get_field($field_name, $this->ID);
  }

  final function get_post_meta($field_name, $single = TRUE)
  {
    return get_post_meta($this->ID, $field_name, $single);
  }

  final function has_sub_field($field_name)
  {
    return has_sub_field($field_name, $this->ID);
  }

  final function update_post_meta($field_name, $value)
  {
    update_post_meta($this->ID, $field_name, $value);
  }

  final function loadObject()
  {
    $this->_picol_object = get_post($this->ID);
  }

  final function saveChanges()
  {
    foreach ($this->_edited_fields as $field => $value) {
      $this->update_post_meta($field, $value);
    }

    $this->_edited_fields = array();
    $this->_cached_fields = array();
  }

  final function enableWriteMode()
  {
    $this->_picol_mode = 'w';
  }

  final function _shutDownCleaning()
  {

    if (!empty($this->_edited_fields)) {
      $campos_nombres = array();
      foreach ($this->_edited_fields as $name => $value) {
        $campos_nombres[] = $name;
      }


      wp_die(sprintf('ERROR: Hay objeto de tipo ' . $this->post_type . ' en modo lectura con cambios asignados. Campos: ' . implode(', ', $campos_nombres)));
    }

  }

}