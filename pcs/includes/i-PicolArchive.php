<?php
class PicolArchive{


  static function getFromPostDate($post_type) {
    /**
     * @var wpdb $wpdb
     */
    global $wpdb;

    $query = "
            SELECT YEAR(post_date) as anho, MONTH (post_date) as mes, count(*) as cuenta FROM
              {$wpdb->posts}
            WHERE
              post_type = '{$post_type}'
              AND post_status = 'publish'
            GROUP BY CONCAT(YEAR(post_date), MONTH(post_date))
            ORDER BY post_date DESC
        ";

    $results = $wpdb->get_results($query);


    return self::prepareArray($results);
  }


  static function getFromPostMeta ($post_type, $meta_key) {
    /**
     * @var wpdb $wpdb
     */
    global $wpdb;

    $query = "
            SELECT YEAR(meta_value) as anho, MONTH (meta_value) as mes, count(*) as cuenta FROM
              {$wpdb->posts}
              JOIN
              {$wpdb->postmeta}
              ON
              ID = post_id
            WHERE
              post_type = '{$post_type}'
              AND post_status = 'publish'
              AND meta_key = '{$meta_key}'
            GROUP BY CONCAT(YEAR(meta_value), MONTH(meta_value))
            ORDER BY meta_value DESC
        ";

    $results = $wpdb->get_results($query);

    return self::prepareArray($results);
  }


  static function getFromTermSlug( $post_type, $term_slug, $taxonomy) {
    /**
     * @var wpdb $wpdbs
     */
    global $wpdb;

    $query = "
            SELECT YEAR(post_date) as anho, MONTH (post_date) as mes, count(*) as cuenta FROM
              {$wpdb->posts}
              JOIN
              {$wpdb->prefix}term_relationships as tr ON ID = object_id
              JOIN
              {$wpdb->prefix}term_taxonomy as tt ON tt.term_taxonomy_id = tr.term_taxonomy_id
              JOIN
              {$wpdb->prefix}terms as t ON t.term_id = tt.term_id

            WHERE
              post_type = '{$post_type}'
              AND post_status = 'publish'
              AND t.slug = '{$term_slug}'
              AND tt.taxonomy = '{$taxonomy}'
            GROUP BY CONCAT(YEAR(post_date), MONTH(post_date))
            ORDER BY post_date DESC
        ";

    $results = $wpdb->get_results($query);

    return self::prepareArray($results);
  }

  protected static function prepareArray($results){
    $archivos = array();
    foreach($results as $un_resultado){
      if(empty($un_resultado->anho)){
        continue;
      }
      $archivos[ $un_resultado->anho ][$un_resultado->mes] = $un_resultado->cuenta;
    }

    return $archivos;
  }

  static function monthName($month){
    $names = array(
      1 => __('January', LANG_DOMAIN),
      2 => __('February', LANG_DOMAIN),
      3 => __('March', LANG_DOMAIN),
      4 => __('April', LANG_DOMAIN),
      5 => __('May', LANG_DOMAIN),
      6 => __('June', LANG_DOMAIN),
      7 => __('July', LANG_DOMAIN),
      8 => __('August', LANG_DOMAIN),
      9 => __('September', LANG_DOMAIN),
      10 => __('October', LANG_DOMAIN),
      11 => __('November', LANG_DOMAIN),
      12 => __('December', LANG_DOMAIN)
    );

    return $names[$month];
  }

}