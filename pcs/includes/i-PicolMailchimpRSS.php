<?php
/**
 * Created by PhpStorm.
 * User: rodrigomurillo
 * Date: 7/16/15
 * Time: 4:21 PM
 */


class PicolMailchimpRSS{

    protected $title;
    protected $content;
    protected $date;

    function __construct($title, $date, $content){
        $this->title = $title;
        $this->content = $content;
        $this->date = $date;
    }

    function printXML() {

        $format = '<?xml version="1.0" encoding="utf-8"?>
            <rss version="2.0">
                <channel>
                    <title>%1$s</title>
                    <lastBuildDate>%2$s</lastBuildDate>
                    <description></description>
                    <item>
                        <title></title>
                        <link></link>
                        <guid></guid>
                        <pubDate>%2$s</pubDate>
                        <content:encoded>
                            <![CDATA[ <!-- -->
                                %3$s
                            ]]>
                        </content:encoded>
                    </item>

                </channel>
            </rss>';

        printf(
            $format,
            $this->title,
            date('D, d M Y H:i:s O',strtotime($this->date)),
            $this->content
        );
    }

}


if(filter_input(INPUT_GET, 'mailchimp-rss-test')){
    $var = new PicolMailchimpRSS('Hola', 'Esta es una prueba', '<table></table>');
    $var->printXML();
}