<?php
/**
 * Created by PhpStorm.
 * User: rodrigomurillo
 * Date: 2/3/15
 * Time: 12:26 PM
 */

class PicolMetaTagger{

    protected $title;
    protected $description;
    protected $keywords;

    protected $image;

    protected $tw_creator;
    protected $tw_site;
    protected $tw_card;
    protected $tw_on = FALSE;

    protected $og_type;
    protected $og_url;
    protected $og_site_name;
    protected $og_on = FALSE;
    protected $og_siteurl;

    protected $fb_appid;

    function __construct( $site_url , $title = '', $description = '',  $keywords = ''){
        if(empty($title)){
            $title = wp_title( '-', false, 'right' ) . get_bloginfo( 'name' );
        }
        $this->description = $description;
        $this->title = $title;
        $this->keywords = $keywords;

        // Defaults
        $this->og_url = $site_url;
        $this->og_type = 'website';
    }

    function setTitle($title){
        $this->title = $title;
    }

    function getTitle(){
        return $this->title;
    }

    function setDescription($description){
        $this->description = $description;
    }

    function getDescription(){
        return $this->description;
    }

    function setImage( $image ) {
        $this->image = $image;
    }
    function setUrl( $url ) {
        $this->og_url = $url;
    }

    function setupTwitterData($creator, $site = '', $card = 'summary_large_image'){

        if(empty($site)){
            $site = $creator;
        }

        $this->tw_card = $card;
        $this->tw_site = $site;
        $this->tw_creator = $creator;



        $this->tw_on = TRUE;
    }

    function setSiteName($site_name = ''){
        $this->og_site_name = $site_name;
    }

    function setType($type = ''){
        $this->og_type = $type;
    }

    function setupOgData($site_name = '', $type = 'website'){
        $this->og_site_name = $site_name;
        $this->og_type = $type;

        $this->og_on = TRUE;
    }

    function printBasicMeta(){
        printf('<title>%1$s</title>', $this->title);
        if(!empty($this->description)){
            printf('<meta name="description" content="%1$s" />', $this->description);
        }
        if(!empty($this->keywords)){
            printf('<meta name="keywords" content="%1$s">', $this->keywords);
        }
    }

    function printTwitterMeta(){
        if(!$this->tw_on)
            return;

        if(!empty($this->tw_card))
            printf('<meta name="twitter:card" content="%1$s">', $this->tw_card);

        if(!empty($this->tw_site))
            printf('<meta name="twitter:site" content="%1$s">', $this->tw_site);

        if(!empty($this->title))
            printf('<meta name="twitter:title" content="%1$s">', $this->title);

        if(!empty($this->description))
            printf('<meta name="twitter:description" content="%1$s">', $this->description);

        if(!empty($this->tw_creator))
            printf('<meta name="twitter:creator" content="%1$s">', $this->tw_creator);

        if(!empty($this->image))
            printf('<meta name="twitter:image:src" content="%1$s">', $this->image);

    }

    function printOpenGraphMeta(){

        if(!empty($this->title))
            printf('<meta property="og:title" content="%1$s" />', $this->title);

        if(!empty($this->og_type))
            printf('<meta property="og:type" content="%1$s" />', $this->og_type);

        if(!empty($this->og_url))
            printf('<meta property="og:url" content="%1$s" />', $this->og_url);

        if(!empty($this->image))
            printf('<meta property="og:image" content="%1$s" />', $this->image);

        if(!empty($this->description))
            printf('<meta property="og:description" content="%1$s" />', $this->description);

        if(!empty($this->og_site_name))
            printf('<meta property="og:site_name" content="%1$s" />', $this->og_site_name);

        if(!empty($this->fb_appid))
            printf('<meta property="fb:app_id" content="%1$s"/>', $this->fb_appid);

    }

    function printAll(){
        $this->printBasicMeta();
        $this->printTwitterMeta();
        $this->printOpenGraphMeta();
    }

}
