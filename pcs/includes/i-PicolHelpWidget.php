<?php
/**
 * Created by PhpStorm.
 * User: rodrigomurillo
 * Date: 7/22/15
 * Time: 7:30 PM
 */

class PicolHelpWidget{

    protected $title, $guide_name, $code_name, $color;
    static protected $is_setup;
    static protected $widgets;

    protected static function setupStyles(){
        if(!isset(self::$is_setup)){
            self::$is_setup = TRUE;
        }

        add_action('admin_head', array('PicolHelpWidget', '_printStyles'));
        add_action('wp_dashboard_setup', array('PicolHelpWidget', '_registerWidgets'));
    }

    static function _registerWidgets(){
        if(!empty(self::$widgets)){
            foreach(self::$widgets as $one_widg){
                $one_widg->registerWidget();
            }
        }
    }

    static function _printStyles(){
        echo '<style type="text/css">
            .picol-help-widget__title{
            padding: 50px 30px;
            border-radius: 4px;
            margin-bottom: 20px;
            background: black;
            color: white;
            font-size: 42px;
            font-family: Georgia, "Times New Roman", Times, serif;
            line-height: 24px;
            text-align: center;
            }

            .picol-help-widget{
            margin-left: 0 !important;
            }

            .picol-help-widget__button{
            text-align: center;
            padding-bottom: 20px;
            }
        </style>';
    }

    function __construct($title, $guide_name, $code_name, $hex_color_code){

        $this->title = $title;
        $this->guide_name = $guide_name;
        $this->code_name = $code_name;
        $this->color = $hex_color_code;

        self::setupStyles();
        self::$widgets[] = $this;
    }

    function registerWidget(){
        wp_add_dashboard_widget(sanitize_title($this->title), $this->title, array(&$this, '_printWidget'));
    }

    function _printWidget(){
        ?>
        <div class="welcome-panel-content picol-help-widget">
            <div class="picol-help-widget__title" style="background: <?php echo $this->color; ?>;">
                <?php echo $this->guide_name; ?>
            </div>
            <div class="picol-help-widget__button">
                <a class="button button-primary button-large" target="_blank" href="http://s.picolestudio.pe/ayuda/<?php echo $this->code_name; ?>/">Abrir esta guía</a>
            </div>
        </div>
        <?php
    }
}