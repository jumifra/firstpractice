<?php
/**
 * Created by PhpStorm.
 * User: rodrigomurillo
 * Date: 5/23/15
 * Time: 6:22 PM
 */

class PicolCounter{
    protected $charges;
    protected $initial_charges;

    function __construct($charges){
        $this->charges = $charges;
        $this->initial_charges = $charges;
    }

    function reset(){
        $this->charges = $this->initial_charges;
    }

    function haveCharges(){
        $have_charges = $this->charges > 0;
        $this->tick();

        if($this->charges == 0) {
            $this->reset();
        }
        return $have_charges;
    }

    protected function tick(){
        $this->charges --;
    }

    function hasAtMost($n){
        return $this->charges <= $n;
    }

    function hasAtLeast($n){
        return $this->charges >= $n;
    }

    function hasMoreThan($n){
        return $this->charges > $n;
    }

    function hasLessThan($n){
        return $this->charges < $n;
    }
}