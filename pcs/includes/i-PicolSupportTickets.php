<?php
/**
 * Created by PhpStorm.
 * User: rodrigomurillo
 * Date: 11/14/14
 * Time: 2:13 AM
 */

defined('ABSPATH') or die("No script kiddies please!");

class PicolSupportTickets extends PicolModule{

    static $trelloApiKey;

    static function init(){
        add_action('wp_dashboard_setup', array('PicolSupportTickets', 'registerVideoWidget'));
        add_action('admin_head', array('PicolSupportTickets', 'printStyles'));
    }

    static function registerVideoWidget(){
        wp_add_dashboard_widget('picol_video_tuts', 'Guía del usuario', array('PicolVideoTuts', 'showWidget'));
    }

    static function printStyles(){
        echo '<style type="text/css">

        .p-videotuts__video{
            display: none;
        }
        .p-videotuts__video:first-child{
            display: block;
        }

        .p-videotuts__title{
            font-size: 15px !important;
            color: rgb(108, 161, 221);
            text-transform: uppercase;
        }

        .p-videotuts__player{
            position: relative;
            height: 0;
            padding-bottom: 56%;
            margin-bottom: 20px;
        }

        .p-videotuts__player iframe,
        .p-videotuts__player embed,
        .p-videotuts__player object{
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }

        .p-videotuts__list li{
            border-top: 1px solid #ddd;
            padding: 10px 0;
            margin: 0;
        }
        .p-videotuts__list li:first-child{
            border: none;
        }
        </style>';
    }
}