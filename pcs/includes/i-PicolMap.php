<?php

class PicolMap{
    static $api_key;
    static function setupMap( $api_key ) {
        self::$api_key = $api_key;
        add_action('acf/init', array(__CLASS__, 'acfInitHook'));
    }

    static function acfInitHook() {
        acf_update_setting('google_api_key', self::$api_key );
    }

    static function printGoogleMapsScript(){
        printf('<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp\&key=%1$s"></script>', self::$api_key );
    }

    static function printMap($map_data, $zoom = 17, $icon_path = null ){

        $icon_attr = '';
        if( $icon_path ) {
            $icon_attr = sprintf('data-icon="%1$s"', TEMPLATE_URL . '/' . $icon_path );
        }

        printf(
            '<div class="picolMap" data-lat="%1$s" data-lng="%2$s" data-zoom="%3$s" %4$s></div>',
            $map_data['lng'],
            $map_data['lng'],
            $zoom,
            $icon_attr
        );
    }
}