<?php
/**
 * Created by PhpStorm.
 * User: rodrigomurillo
 * Date: 7/2/15
 * Time: 7:42 PM
 */

class PicolSharer{

    protected $url;
    protected $title;
    protected $encoded_url;
    protected $encoded_title;

    function __construct($url, $title){
        $this->url;
        $this->title;

        $this->encoded_title = urlencode($title);
        $this->encoded_url = urlencode($url);
    }


    protected function prepareShareUrl($url){
        return str_replace(array(
            '[url]',
            '[title]'
        ), array(
            $this->encoded_url,
            $this->encoded_title
        ),$url);
    }

    function getFacebookUrl(){
        return $this->prepareShareUrl('//www.facebook.com/sharer/sharer.php?u=[url]');
    }

    function getGooglePlusUrl(){
        return $this->prepareShareUrl('//plus.google.com/share?url=[url]');
    }

    function getTwitterUrl(){
        return $this->prepareShareUrl('//www.twitter.com/intent/tweet?url=[url]&text=[title]');
    }

    function getLinkedInUrl(){
        return $this->prepareShareUrl('//www.linkedin.com/shareArticle?mini=true&url=[url]&title=[title]');
    }

    function getEmailShareUrl(){
        return $this->prepareShareUrl('mailto:?subject=[title]&body=[url]');
    }
}