<?php

class PicolAutoloader{

    protected $folder;
    protected $prefix;

    function __construct( $folder, $prefix = '') {
        $this->folder = $folder;
        $this->prefix = $prefix;

        spl_autoload_register(array($this, '__autoload'));
    }

    function __autoload( $classname ) {
        $filename = untrailingslashit($this->folder) . '/' . $this->prefix . $classname . '.php';
        if(file_exists($filename)){
            include_once $filename;
        }
    }

    function initAll(){

        $prefix_length = strlen($this->prefix);
        $suffix_length = strlen('.php');

        $modules = glob( untrailingslashit($this->folder) . '/*.php' );

        // Call init
        foreach($modules as $module_file ){
            $basename = basename($module_file);
            $class_name_length = strlen($basename) - $prefix_length - $suffix_length;
            $class_name = substr( $basename, $prefix_length, $class_name_length );
            $callable = array($class_name, 'init');

            if(is_callable($callable)){
                call_user_func($callable);
            }
        }
    }


}