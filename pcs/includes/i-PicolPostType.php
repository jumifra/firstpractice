<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rodrigomurillo
 * Date: 6/29/13
 * Time: 12:34 PM
 *
 * v 0.0.1
 * 23/12/13
 */

defined('ABSPATH') or die("No script kiddies please!");

class PicolPostType {

    protected static $is_class_setup;

    protected $_labels_setup = FALSE;
    protected $_post_type;
    protected $_slug;
    protected $_args;

    protected $label = '[Custom Post Label]';
    public $description = '';

    /**
     * @var bool
     * @default FALSE
     */
    public $has_archive = FALSE;

    /**
     * @var bool
     * @default TRUE
     */
    public $public = TRUE;

    /**
     * @var bool
     * @default TRUE
     */
    public $show_ui = TRUE;

    /**
     * @var bool
     * @default TRUE
     */
    public $show_in_menu = TRUE;

    /**
     * @var bool
     * @default null
     */
    public $menu_position = null;

    /**
     * @var string
     * @default 'post'
     */
    public $capability_type = 'post';

    /**
     * @var string
     * @default FALSE
     */
    public $hierarchical = FALSE;

    /**
     * @var bool
     * @default TRUE
     */
    public $query_var = TRUE;

    /**
     * @var bool
     * @default FALSE
     */
    public $exclude_from_search = FALSE;

    /**
     * @var array
     * @default array('title', 'editor');
     */
    public $supports = array('title');

    /**
     * @var array
     * @default array()
     */

    public $taxonomies = array();

    public $menu_icon;

    /*
     * FOR COLUMNS
     * --------------------------------------------------------------------------------
     */

    protected $_columns = array();

    /*
     * FOR SAVING A POST
     * --------------------------------------------------------------------------------
     */

    protected $_savePublishedActions = array();



    /*
     * FOR LATER REFERENCE
     * --------------------------------------------------------------------------------
     */

    protected static $post_types = array();


    /*
     * LABELS
     * --------------------------------------------------------------------------------
     */

    public $l_singular_name;
    public $l_add_new;
    public $l_add_new_item;
    public $l_edit;
    public $l_edit_item;
    public $l_menu_name;
    public $l_name;
    public $l_not_found;
    public $l_not_found_in_trash;
    public $l_parent;
    public $l_parent_colon;
    public $l_search_items;
    public $l_view;
    public $l_view_item;

    /*
     * Enter Title Here
     * --------------------------------------------------------------------------------
     */
    public $enter_title_here;

    /**
     * @param $post_type
     * @return PicolPostType
     */

    static function getObject( $post_type ) {
        if(isset(self::$post_types[$post_type])){
            return self::$post_types[$post_type];
        }

        return FALSE;
    }

    /**
     * Things that should be run just once
     */

    static function classSetup(){

        if(!isset(self::$is_class_setup)){
            add_filter('enter_title_here', array('PicolPostType', '_enterTitleHereFilter'));

            self::$is_class_setup = TRUE;
        }

    }

    static function _enterTitleHereFilter( $enter_title_here ){

        $screen = get_current_screen();

        if(isset(self::$post_types[ $screen->post_type ])){
            if( self::$post_types[ $screen->post_type ]->enter_title_here ) {
                return self::$post_types[ $screen->post_type ]->enter_title_here;
            }
        }

        return $enter_title_here;

    }


    /*
     * FUNCTIONS
     * --------------------------------------------------------------------------------
     */

    function __construct($post_name, $slug){

        $this->_post_type = $post_name;
        $this->_slug = $slug;

        add_action('init', array( &$this, '_doRegisterPostType'));

        self::$post_types[ $this->_post_type ] = $this;
        self::classSetup();

    }

    final function autoLabels( $singular, $plural, $masculino = TRUE, $menu_name = null ){

        $masculine_str = array(
            'add_new'            => _x('Add new', 'masculine post_type', LANG_DOMAIN),
            'add_new_item'       => _x('Add new %s', 'masculine post_type', LANG_DOMAIN),
            'edit'               => _x('Edit', 'masculine post type', LANG_DOMAIN),
            'edit_item'          => _x('Edit %s:', 'masculine post_type', LANG_DOMAIN),
            'not_found'          => _x('%s not found', 'masculine post_type', LANG_DOMAIN),
            'not_found_in_trash' => _x('%s not found in trash', 'masculine post_type', LANG_DOMAIN),
            'parent'             => _x('Parent %s', 'masculine post_type', LANG_DOMAIN),
            'parent_colon'       => _x('Parent %s:', 'masculine post_type', LANG_DOMAIN),
            'search_items'       => _x('Search %s', 'masculine post_type', LANG_DOMAIN),
            'view'               => _x('View', 'masculine post_type', LANG_DOMAIN),
            'view_item'          => _x('View %s:', 'masculine post_type', LANG_DOMAIN)
        );

        $feminine_str = array(
            'add_new'            => _x('Add new', 'femenine post_type', LANG_DOMAIN),
            'add_new_item'       => _x('Add new %s', 'femenine post_type', LANG_DOMAIN),
            'edit'               => _x('Edit', 'femenine post type', LANG_DOMAIN),
            'edit_item'          => _x('Edit %s:', 'femenine post_type', LANG_DOMAIN),
            'not_found'          => _x('%s not found', 'femenine post_type', LANG_DOMAIN),
            'not_found_in_trash' => _x('%s not found in trash', 'femenine post_type', LANG_DOMAIN),
            'parent'             => _x('Parent %s', 'femenine post_type', LANG_DOMAIN),
            'parent_colon'       => _x('Parent %s:', 'femenine post_type', LANG_DOMAIN),
            'search_items'       => _x('Search %s', 'femenine post_type', LANG_DOMAIN),
            'view'               => _x('View', 'femenine post_type', LANG_DOMAIN),
            'view_item'          => _x('View %s:', 'femenine post_type', LANG_DOMAIN)
        );

        $str_set = $masculino ? $masculine_str : $feminine_str;

        $label = !is_null( $menu_name ) ? $menu_name : $plural;

        $this->label = $label;
        $this->_labels_setup = TRUE;

        $this->l_singular_name      = $singular;
        $this->l_add_new            = $str_set['add_new'];
        $this->l_add_new_item       = sprintf($str_set['add_new_item'], $singular);
        $this->l_edit               = $str_set['edit'];
        $this->l_edit_item          = sprintf($str_set['edit_item'], $singular);
        $this->l_menu_name          = $label;
        $this->l_name               = $plural;
        $this->l_not_found          = sprintf($str_set['not_found'], $plural);
        $this->l_not_found_in_trash = sprintf($str_set['not_found_in_trash'], $plural);
        $this->l_parent             = sprintf($str_set['parent'], $singular);
        $this->l_parent_colon       = sprintf($str_set['parent_colon'], $singular);
        $this->l_search_items       = sprintf($str_set['search_items'], $plural);
        $this->l_view               = sprintf($str_set['view'], $singular);
        $this->l_view_item          = sprintf($str_set['view_item'], $singular);

        return $this;
    }

    final function _doRegisterPostType(){

        $args = array(
            'label' => $this->label,
            'description' => $this->description,
            'has_archive' => $this->has_archive,
            'public' => $this->public,
            'show_ui' => $this->show_ui,
            'show_in_menu' => $this->show_in_menu,
            'menu_position' => $this->menu_position,
            'capability_type' => $this->capability_type,
            'hierarchical' => $this->hierarchical,

            'rewrite' => array(
                'slug' => $this->_slug
            ),

            'taxonomies' => $this->taxonomies,

            'query_var' => $this->query_var,
            'exclude_from_search' => $this->exclude_from_search,
            'supports' => $this->supports,

            'labels' => array(
                'singular_name'      => $this->l_singular_name,
                'add_new'            => $this->l_add_new_item,
                'add_new_item'       => $this->l_add_new_item,
                'edit'               => $this->l_edit,
                'edit_item'          => $this->l_edit_item,
                'menu_name'          => $this->l_menu_name,
                'name'               => $this->l_name,
                'not_found'          => $this->l_not_found,
                'not_found_in_trash' => $this->l_not_found_in_trash,
                'parent'             => $this->l_parent,
                'parent_colon'       => $this->l_parent_colon,
                'search_items'       => $this->l_search_items,
                'view'               => $this->l_view,
                'view_item'          => $this->l_view_item
            )
        );

        $args['menu_icon'] = $this->menu_icon;

        register_post_type($this->_post_type, $args);

    }

    final function getPostTypeName(){
        return $this->_post_type;
    }

}