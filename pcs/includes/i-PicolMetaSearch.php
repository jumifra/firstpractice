<?php

class PicolMetaSearch{

    /*
     * Modificar el SQL de la búsqueda
     */

    static function enableMetaSearch(){
        add_filter('posts_join', array(__CLASS__, '_postsJoin') );
        add_filter('posts_where', array(__CLASS__, '_postsWhere') );
        add_filter('posts_distinct', array(__CLASS__, '_distinct') );
    }

    static function disableMetaSearch(){
        remove_filter('posts_join', array(__CLASS__, '_postsJoin') );
        remove_filter('posts_where', array(__CLASS__, '_postsWhere') );
        remove_filter('posts_distinct', array(__CLASS__, '_distinct') );
    }

    static function _postsJoin( $join ) {
        global $wpdb;
        $join .=' LEFT JOIN '.$wpdb->postmeta. ' as search_meta ON '. $wpdb->posts . '.ID = search_meta.post_id ';
        return $join;
    }

    static function _postsWhere( $where ) {
        global $wpdb;

        $where = preg_replace(
            "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
            "(".$wpdb->posts.".post_title LIKE $1) OR (search_meta.meta_value LIKE $1)", $where );

        return $where;
    }

    static function _distinct( $where ) {
        return "DISTINCT";
    }

}