<?php
/**
 * Created by PhpStorm.
 * User: rodrigomurillo
 * Date: 12/4/14
 * Time: 7:14 PM
 */

class PicolStyleInliner{

    protected $contents;

    function __construct( $filename_or_contents ) {

        $contents = $filename_or_contents;

        if( file_exists( $filename_or_contents ) ) {
            $contents = file_get_contents( $filename_or_contents );
        }

        // Remove style tag

        $clean_contents = preg_replace('/<style[^>]*>[^<]*<\/style>/', '', $contents);

        $this->contents = $clean_contents;

    }

    function addStyles( $filename_or_styles ) {
        $stylesheet = $filename_or_styles;

        $contents = $this->contents;

        if( file_exists( $filename_or_styles ) ) {
            $stylesheet = file_get_contents( $filename_or_styles );
        }

        // Capture all styles

        preg_match_all('/\.([^{^}]+){([^}]+)}/', $stylesheet, $style_matches );
        // Replace classes with styles


        for ( $i = 0, $length = count( $style_matches[1]); $i < $length; $i ++ ) {
            $rule          = trim( $style_matches[1][$i] );
            $properties    = $style_matches[2][$i];
            $inline_styles = str_replace(array( "  ", "\n", "\r" ), '', $properties);
            $contents      = str_replace('class="'. $rule .'"', 'style="' . $inline_styles . '"', $contents );
        }

        $this->contents = $contents;
    }

    /**
     * @return mixed
     */

    public function getContents(){
        return $this->contents;
    }

}