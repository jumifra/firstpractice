<?php
/**
 * Created by PhpStorm.
 * User: rodrigomurillo
 * Date: 12/3/14
 * Time: 10:46 PM
 */

defined('ABSPATH') or die("No script kiddies please!");

class PicolUpdater{

    const PLUGIN = 'plugin';
    const THEME = 'theme';

    protected $file;
    protected $type;
    protected $update_server;
    protected $plugin_slug;

    protected $theme_slug;
    protected $theme_version;

    protected static $updaters;

    protected static function setup(){
        if(!is_array( self::$updaters )){
            self::$updaters = array();
        }
    }

    static function plugin( $__file__, $update_server ){
        new PicolUpdater($__file__, $update_server, self::PLUGIN);
    }

    static function theme( $__file__, $update_server ) {
        new PicolUpdater($__file__, $update_server, self::THEME );
    }

    protected function __construct( $__file__, $update_server, $type ){

        self::setup();

        $this->file = $__file__;
        $this->update_server = $update_server;

        $this->plugin_slug = basename(dirname($__file__));

        if( $type == self::PLUGIN ){
            add_filter('pre_set_site_transient_update_plugins', array(&$this, '_pluginCheck'));
            add_filter('plugins_api', array(&$this, '_pluginApiCall'), 10, 3);
        } else if( $type == self::THEME ) {
            add_filter('pre_set_site_transient_update_themes', array(&$this, '_themeCheck'));
            add_filter('themes_api', array(&$this, '_themeApiCall'), 10, 3);

            $this->setupTheme();
        }

        $this->type = $type;
        self::$updaters[] = $this;
    }

    protected function setupTheme(){

        if(function_exists('wp_get_theme')){
            $theme_data = wp_get_theme(get_option('template'));
            $theme_version = $theme_data->Version;
        } else {
            $theme_data = get_theme_data( TEMPLATEPATH . '/style.css');
            $theme_version = $theme_data['Version'];
        }

        $theme_base = get_option('template');
        $this->theme_version = $theme_version;
        $this->theme_slug = $theme_base;

        if (is_admin())
            $current = get_transient('update_themes');
    }

    function _pluginCheck( $checked_data ){

        $plugin_slug   = $this->plugin_slug;
        $plugin_file   = $plugin_slug . '/' . $plugin_slug . '.php';
        $action        = 'basic_check';

        //Comment out these two lines during testing.
        if (empty($checked_data->checked))
            return $checked_data;

        $plugin_version = $checked_data->checked[$plugin_file];

        $this->prepareCheckedData($checked_data, $plugin_slug, $plugin_version, $action, $plugin_file );

        return $checked_data;
    }

    function _themeCheck($checked_data) {

        $theme_version = $this->theme_version;
        $theme_slug = $this->theme_slug;
        $action = 'theme_update';

        $this->prepareCheckedData($checked_data, $theme_slug, $theme_version, $action );

        return $checked_data;
    }

    protected function prepareCheckedData( &$checked_data, $slug, $version, $action, $response_slug = '' ){

        global $wp_version;

        $update_server = $this->update_server;

        if(empty($response_slug))
            $response_slug = $slug;

        $request = array(
            'slug' => $slug,
            'version' => $version,
        );

        $request_string = array(
            'body' => array(
                'action' => $action,
                'request' => serialize($request),
                'api-key' => md5(get_bloginfo('url'))
            ),
            'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')
        );

        // Start checking for an update
        $raw_response = wp_remote_post($update_server, $request_string);

        if (!is_wp_error($raw_response) && ($raw_response['response']['code'] == 200)){
            $response = unserialize($raw_response['body']);
        }

        if( $this->type == self::PLUGIN){
            $all_ok = is_object($response) && !empty($response);
        } else {
            $all_ok = !empty($response);
        }

        if ($all_ok) // Feed the update data into WP updater
            $checked_data->response[ $response_slug ] = $response;

    }

    function _pluginApiCall($def, $action, $args) {
        global $wp_version;

        $plugin_slug = $this->plugin_slug;
        $update_server = $this->update_server;

        if (!isset($args->slug) || ($args->slug != $plugin_slug))
            return false;

        // Get the current version
        $plugin_info = get_site_transient('update_plugins');
        $current_version = $plugin_info->checked[$plugin_slug .'/'. $plugin_slug .'.php'];
        $args->version = $current_version;

        $request_string = array(
            'body' => array(
                'action' => $action,
                'request' => serialize($args),
                'api-key' => md5(get_bloginfo('url'))
            ),
            'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')
        );

        $request = wp_remote_post($update_server, $request_string);

        if (is_wp_error($request)) {
            $res = new WP_Error('plugins_api_failed', __('An Unexpected HTTP Error occurred during the API request.</p> <p><a href="?" onclick="document.location.reload(); return false;">Try again</a>'), $request->get_error_message());
        } else {
            $res = unserialize($request['body']);

            if ($res === false)
                $res = new WP_Error('plugins_api_failed', __('An unknown error occurred'), $request['body']);
        }

        return $res;

    }

    function _themeApiCall($def, $action, $args) {

        $theme_slug = $this->theme_slug;
        $theme_version = $this->theme_version;

        $update_server = $this->update_server;

        if ($args->slug != $theme_slug)
            return false;

        // Get the current version

        $args->version = $theme_version;
        $request_string = prepare_request($action, $args);
        $request = wp_remote_post($update_server, $request_string);

        if (is_wp_error($request)) {
            $res = new WP_Error('themes_api_failed', __('An Unexpected HTTP Error occurred during the API request.</p> <p><a href="?" onclick="document.location.reload(); return false;">Try again</a>'), $request->get_error_message());
        } else {
            $res = unserialize($request['body']);

            if ($res === false)
                $res = new WP_Error('themes_api_failed', __('An unknown error occurred'), $request['body']);
        }

        return $res;
    }

}
