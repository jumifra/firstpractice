<?php
/**
 * Created by PhpStorm.
 * User: rodrigomurillo
 * Date: 1/30/15
 * Time: 12:29 PM
 */

class PicolMetaBox{

    protected $id;
    protected $name;
    protected $callback;
    protected $post_types;

    function __construct( $post_types, $id, $name, $callback_or_file ){
        $post_types = (array) $post_types;

        $this->post_types = $post_types;
        $this->id = $id;
        $this->name = $name;
        $this->callback = $callback_or_file;

        foreach($post_types as $post_type) {
            add_action('add_meta_boxes_' . $post_type, array(&$this, '_registerMetaBox'));
        }
    }

    function _registerMetaBox(){

        $callback = FALSE;

        if(is_callable($this->callback)){
            $callback = $this->callback;
        }else{
            $template_rel_file = get_stylesheet_directory() . '/' . $this->callback;
            $absolute_path_file = $this->callback;

            if(file_exists( $template_rel_file )){
                $this->callback = $template_rel_file;
                $callback = array(&$this, '_doTheFile');
            } else if( file_exists( $absolute_path_file )){
                $this->callback = $absolute_path_file;
                $callback = array(&$this, '_doTheFile');
            }
        }

        if($callback){

            foreach($this->post_types as $post_type) {
                add_meta_box(
                    $this->id,
                    $this->name,
                    $callback,
                    $post_type,
                    'advanced'
                );
            }

        }else {
            throw new Exception(__('No se especificó un callback válido para PicolMetaBox'));
        }

    }

    function _doTheFile( $post ){
        global $post;

        $file = $this->callback;
        include $file;

    }
}