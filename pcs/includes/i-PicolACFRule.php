<?php

class PicolACFRule{

  protected $rules_group;
  protected $rule_key;
  protected $rule_label;
  protected $rule_values;
  protected $rule_match_callback;
  protected $rule_match_ajax_callback;

  function __construct($rules_group, $rule_key, $rule_label, $rule_values, $rule_match_callback, $rule_match_ajax_callback = null ){

    $this->rules_group         = $rules_group;
    $this->rule_key            = $rule_key;
    $this->rule_label          = $rule_label;
    $this->rule_values         = $rule_values;
    $this->rule_match_callback = $rule_match_callback;

    if( !is_array($rule_values) ){
      throw new Exception('PicolACFRules necesita un array como valores del parámetro $rule_values');
    }

    if( !is_callable( $rule_match_callback )){
      throw new Exception('PicolACFRules necesita un callback válido como valor del parámetro $rule_match_callback');
    }

    add_filter('acf/location/rule_types', array($this, '_ruleTypes'));
    add_filter('acf/location/rule_values/' . $rule_key , array($this, '_ruleValues'));
    add_filter('acf/location/rule_match/' . $rule_key , array($this, '_ruleMatching'), 10, 3);

  }

  function _ruleTypes( $choices ){
    $choices[ $this->rules_group ][ $this->rule_key ] = $this->rule_label;
    return $choices;
  }

  function _ruleValues( $choices ){

    foreach( $this->rule_values as $key => $value ) {
      $choices[ $key ] = $value;
    }

    return $choices;

  }

  function _ruleMatching( $match, $rule, $options ){
      if(isset($options['action']) && $options['action'] == 'acf/post/get_field_groups'){
          $post_id                 = _p('post_id');
          $options['post_type'] = get_post_type($post_id);
      }

    $matches = call_user_func_array( $this->rule_match_callback, array(
      $match,
      $rule,
      $options
    ) );

    return $matches;
  }

}