<?php
/**
 * Created by PhpStorm.
 * User: rodrigomurillo
 * Date: 12/4/14
 * Time: 5:38 PM
 *
 * @deprecated use PicolTwig instead
 */

class PicolTemplate{
    protected $context;
    protected $template_contents;

    /**
     * PicolTemplate @var
     */
    protected $parent_template;

    /**
     * @param string $template_contents
     * @param PicolTemplate $parent_template
     */
    function __construct($template_contents = '', $parent_template = null){

        $this->context = new stdClass();

        if(!empty($template_contents)){
            $this->template_contents = $template_contents;
        }

        if(!empty($parent_template)){
            $this->parent_template = $parent_template;
        }

    }

    function setContext( $context ) {
        $this->context = $context;
    }

    function setTemplateContents( $contents ) {
        $this->template_contents = $contents;
    }

    function getContext(){
        if(is_array( $this->context )){
            $new_context = new stdClass();
            foreach( $this->context as $name => $value ){
                $new_context->{$name} = $value;
            }
            $this->context = $new_context;
        }
        return $this->context;
    }

    /**
     * @return string
     */

    function getContents(){

        $this->prepareLoops();

        $string = $this->template_contents;

        preg_match_all('/{([\w]+\.?[\w]*)}/', $string, $matches);

        $calls = $matches[0];
        $names = $matches[1];

        for( $i = 0, $size = count($calls); $i < $size; $i ++){
            $string = str_replace( $calls[$i], $this->getValue( $names[$i] ), $string);
        }
        return $string;
    }

    protected function prepareLoops(){
        $loop_pattern = '/{foreach\s([\w]+\.*[\w]*)\sas\s([\w]+)}(.*){\/foreach \1}/s';
        $contextualize_replacement = '{\1\2\3}';

        $current_context = $this->getContext();


        // Find loops

        preg_match_all($loop_pattern, $this->template_contents, $matches );

        $variables = $matches[1];
        $context_names = $matches[2];
        $loop_contents = $matches[3];


        /*
         * For each loop:
         *
         * - Replace loop with {loop_[varname]}
         * - Create a new template
         * - Assign new template to $context->loop_[varname]
         * - Assign the appropriate object as context to each template
         */

        for( $i = 0, $size = count($variables); $i < $size; $i ++){

            $var_name          = $variables[$i];
            $context_name      = $context_names[$i];
            $template_contents = $loop_contents[$i];

            $var_value         = $this->getValue($var_name);

            $replace_pattern   = '/{foreach\s' . $var_name . '\sas\s' . $context_name . '}(.*){\/foreach ' . $var_name . '}/s';

            $loop_strs         = array();

            $contextualize_pattern = '/{(.*)\b' . $context_name . '\.([\w]+)\b(.*)}/';

            // Contextualize loop (transforms {item.property} into {property} for the new context

            $template_contents = preg_replace( $contextualize_pattern, $contextualize_replacement, $template_contents );

            // Create as many templates as items in this variable
            if( is_array( $var_value )) {
                for( $j = 0, $var_length = count($var_value); $j < $var_length; $j ++ ){

                    $j_value   = $var_value[$j];
                    $loop_name = 'loop_' . $var_name . '_' . $j;
                    $template  = new PicolTemplate($template_contents, $this);

                    if( is_array( $j_value) || is_object( $j_value )) {
                        $template->setContext($j_value);
                    } else {
                        $context = $template->getContext();
                        $context->{$context_name} = $j_value;
                    }

                    $current_context->{$loop_name} = $template;

                    $loop_strs[] = $loop_name;
                }
            }

            $replacement = '{' . implode('}{', $loop_strs) . '}';
            $this->template_contents = preg_replace( $replace_pattern, $replacement, $this->template_contents);

        }
    }

    /**
     * @param $var_name
     * @return string
     */

    protected function getValue( $var_name ){

        if( is_object( $this->context ) &&  $this->context->{$var_name} ){

            if($this->context->{$var_name} instanceof PicolTemplate) {
                return $this->context->{$var_name}->getContents();
            }

            return $this->context->{$var_name};

        } else if(is_array( $this->context) && isset( $this->context[ $var_name ])){
            return $this->context[ $var_name ];
        } else if (isset($this->parent_template)){
            return $this->parent_template->getValue( $var_name );
        } else {
            return '';
        }

    }

}