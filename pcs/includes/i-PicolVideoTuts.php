<?php
/**
 * Created by PhpStorm.
 * User: rodrigomurillo
 * Date: 11/14/14
 * Time: 1:18 AM
 */

defined('ABSPATH') or die("No script kiddies please!");

class PicolVideoTuts extends PicolModule{

    protected static $videos;

    static function init(){
        self::$videos = array();

        /* -- Panel de Reclamos * --*/
        add_action('wp_dashboard_setup', array('PicolVideoTuts', 'registerVideoWidget'));
        add_action('admin_head', array('PicolVideoTuts', 'printStyles'));

    }

    static function registerVideoWidget(){
        wp_add_dashboard_widget('picol_video_tuts', 'Guía del usuario', array('PicolVideoTuts', 'showWidget'));
    }

    static function addVideo( $name, $video_url ) {
        self::$videos[ $name ] = $video_url;
    }

    static function showWidget(){
        $html = '<div class="p-videotuts">
            <div class="p-videotuts__videos">
                %1$s
            </div>
            <ul class="p-videotuts__list">
                %2$s
            </ul>
        </div>';

        $video_format = '<div class="p-videotuts__video" id="%3$s">
            <h1 class="p-videotuts__title">%1$s</h1>
            <div class="p-videotuts__player">
                %2$s
            </div>
        </div>';

        $list_format = '<li><a href="#%1$s">%2$s</a></li>';


        $videos_str = array();
        $list_str = array();

        foreach(self::$videos as $title => $one_video){

            $name = sanitize_title( $title );
            $video_html = '';

            $videos_str[] = sprintf( $video_format, $title, $video_html );
            $list_str[] = sprintf( $list_format, $name, $title );
        }

        printf( $html, implode('', $videos_str), implode('', $list_str ));

        self::printScripts();

    }

    static function printStyles(){
        echo '<style type="text/css">

        .p-videotuts__video{
            display: none;
        }
        .p-videotuts__video:first-child{
            display: block;
        }

        .p-videotuts__title{
            font-size: 15px !important;
            color: rgb(108, 161, 221);
            text-transform: uppercase;
        }

        .p-videotuts__player{
            position: relative;
            height: 0;
            padding-bottom: 56%;
            margin-bottom: 20px;
        }

        .p-videotuts__player iframe,
        .p-videotuts__player embed,
        .p-videotuts__player object{
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }

        .p-videotuts__list li{
            border-top: 1px solid #ddd;
            padding: 10px 0;
            margin: 0;
        }
        .p-videotuts__list li:first-child{
            border: none;
        }
        </style>';
    }

    static function printScripts(){

    }

}