<?php
/**
 * Created by PhpStorm.
 * User: rodrigomurillo
 * Date: 10/28/14
 * Time: 5:58 PM
 */

defined('ABSPATH') or die("No script kiddies please!");

class PicolTheme{

    protected static $menus;
    protected static $tiny_mce_formats;
    protected static $removed_menu_pages;
    protected static $footer_text;
    protected static $footer_version;

    /*
     * ACF OPTIONS
     */

    static function addOptionsPage($menu_name, $slug = null, $capability = 'edit_posts', $page_title = null, $parent_slug = null, $icon_url = ''){

      $slug = empty($menu_name) ? sanitize_title( $menu_name ) : $slug;
      $page_title = empty($page_title) ? $menu_name : $page_title;

        if( function_exists('acf_add_options_page') ) {

            acf_add_options_page(array(
                'page_title' 	=> $page_title,
                'menu_title'	=> $menu_name,
                'menu_slug' 	=> $slug,
                'capability'	=> $capability,
                'redirect'		=> false,
                'parent_slug'   => $parent_slug,
                'icon_url'      => $icon_url
            ));

        }
    }

    /*
     * NAV MENU
     */

    static function registerMenu($name, $label){
        static $action_set;

        if(!isset($action_set)){
            add_action('init', array( 'PicolTheme', '_registerNavMenus' ));
            self::$menus = array();
            $action_set = TRUE;
        }

        self::$menus[ $name ] = $label;

    }

    static function _registerNavMenus(){
        register_nav_menus( self::$menus );
    }

    static function getMenuItems( $menu_name ) {

        static $items;

        if( isset( $items[ $menu_name ])) {
            return $items[ $menu_name ];
        }

        $locations = get_nav_menu_locations();

        $menu_items = array();

        if ( ( $locations ) && isset( $locations[ $menu_name ] ) ) {

            $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );

            $menu_items = wp_get_nav_menu_items($menu->term_id);

        }

        $items[ $menu_name ] = $menu_items;
        return $menu_items;

    }

    /*
     * ADMIN MENU
     */

    static function adminMenuRemoveItem($admin_url){

        static $action_set;

        if(!isset($action_set)){
            add_action( 'admin_menu', array('PicolTheme', '_doRemoveAdminMenuPages') );
            $action_set = TRUE;
        }

        if(!is_array(self::$removed_menu_pages)){
            self::$removed_menu_pages = array();
        }

        self::$removed_menu_pages[] = $admin_url;
    }

    static function adminMenuRemovePagesItem(){
        self::adminMenuRemoveItem('edit.php?post_type=page');
    }

    static function adminMenuRemoveLinksItem(){
        self::adminMenuRemoveItem('link-manager.php');
    }

    static function adminMenuRemovePostsItem(){
        self::adminMenuRemoveItem('edit.php');
    }

    static function adminMenuRemoveCommentsItem(){
        self::adminMenuRemoveItem('edit-comments.php');
    }

    static function _doRemoveAdminMenuPages(){
        if(is_array(self::$removed_menu_pages)){
            foreach(self::$removed_menu_pages as $page){
                remove_menu_page($page);
            }
        }
    }

    /*
     * THUMBNAILS SUPPORT
     */

    static function supportAddThumbnails(){
        add_theme_support('post-thumbnails');
    }

    static function supportAddPostFormats( $formats = array()){
        add_theme_support('post-formats', $formats);
    }


    /*
     * TINY MCE
     */

    static function tinyMceAddFormat($name, $tag = ''){

        static $action_set;

        if(!isset($action_set)){
            add_filter('tiny_mce_before_init', array('PicolTheme', '_tinyMceFormats'));
            $action_set = TRUE;
        }

        if(!is_array(self::$tiny_mce_formats)){
            self::$tiny_mce_formats = array();
        }

        if(is_array($name)){
           self::$tiny_mce_formats = array_merge(self::$tiny_mce_formats, $name);
        }else{
            self::$tiny_mce_formats[$name] = $tag;
        }
    }

    static function _tinyMceFormats( $init ){

        $format_str = array();
        foreach(self::$tiny_mce_formats as $name => $tag){
            $format_str[] = $name . '=' . $tag;
        }

        $format_str = implode(';', $format_str);

        $init['block_formats'] = $format_str;

        return $init;
    }

    /*
     * WPHEAD
     */

    static function wpHeadResponsive(){
        add_action('wp_head', array('PicolTheme', '_wpHeadResponsive'));
    }

    static function _wpHeadResponsive(){
        echo '<meta name="viewport" content="width=device-width" />';
    }

    static function doNotIndex(){
        add_action('wp_head', array('PicolTheme', '_doNotIndex'));
    }

    static function _doNotIndex(){
        echo '<meta name="robots" content="noindex, nofollow" />';
    }

    static function _doRemoveAllDashboardWidgets(){
        global $wp_meta_boxes;

        unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
        unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
        unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
        unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
        unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
        unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
        unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
        unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
        unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
    }

    static function removeAllDashboardWidgets(){
        add_action('wp_dashboard_setup', array('PicolTheme', '_doRemoveAllDashboardWidgets'));
    }

  static function getThemeVersion(){
    static $theme;

    if(!isset($theme)){
      $theme = wp_get_theme();
    }

    return $theme->version;
  }

    static function replaceAdminFooterText( $text ){
        self::$footer_text = $text;
        add_filter('admin_footer_text', array('PicolTheme','_doReplaceAdminFooterText'));
    }

    static function _doReplaceAdminFooterText( $original_text ){
        if(isset(self::$footer_text)){
            return self::$footer_text;
        }
        return $original_text;
    }

    static function replaceAdminFooterVersion( $text ) {
        self::$footer_version = $text;
        add_filter('update_footer', array('PicolTheme','_doReplaceFooterVersion'), 99);
    }

    static function _doReplaceFooterVersion( $original_text ) {
        if(isset(self::$footer_version)){
            return self::$footer_version;
        }
        return $original_text;
    }


}