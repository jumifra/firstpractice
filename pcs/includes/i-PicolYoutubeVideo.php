<?php
/**
 * Created by PhpStorm.
 * User: rodrigomurillo
 * Date: 6/15/15
 * Time: 7:55 PM
 */

class PicolYoutubeVideo extends PicolVideo{

    function __construct($vid_url) {
        $this->is_youtube = true;
        parent::__construct( $vid_url );
    }

    function getID(){
        return $this->getVideoID();
    }

    function getVideoHTML(){
        $video_format = '//www.youtube.com/embed/%1$s?rel=0&enablejsapi=1';
        $id = $this->getVideoID();

        if ( !$id ) {
            return FALSE;
        }

        $size_attr = $this->getHTMLSizeAttr(null, null);

        $html_format = '<iframe class="embed-responsive-item" src="%1$s" %2$s %3$s frameborder="0" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"></iframe>';

        return sprintf($html_format, sprintf($video_format, $id), $size_attr[0], $size_attr[1]);
    }

    function getThumbUrl(){
        $id = $this->getVideoID();

        if ( $id ) {
            return 'https://img.youtube.com/vi/' . $id . '/hqdefault.jpg';
        }

        return FALSE;
    }

    function getThumbWidth(){
        return 480;
    }

    function getThumbHeight(){
        return 360;
    }

    protected function getVideoID(){

        $formats = array(
            '/youtube\.com\/.*v=([^\/^&]+)/',
            '/youtu\.be\/([^\/^&]+)/'
        );
        foreach( $formats as $one_format ) {
            $any_match = preg_match($one_format, $this->video_url, $matches);
            if($any_match !== FALSE) {
                break;
            }
        }

        if($any_match === FALSE) {
            return FALSE;
        }

        $id = $matches[1];

        return $id;
    }
}