<?php

class PicolTwig{
    static $child_env;
    static $base_env;

    protected function __construct(){

    }

    /**
     * @param $path
     * @return Twig_TemplateInterface
     * @throws Exception
     */

    static function getTwig( $path ){

        $child_twig_dir = get_stylesheet_directory() . '/twig-templates/';
        $base_twig_dir = get_template_directory() . '/twig-templates/';
        $filename = $path . '.twig';

        // Create static environments for Samurai path and for the child theme

        if(!isset(self::$child_env)){
            if(is_dir( $child_twig_dir )) {
                $child_loader = new Twig_Loader_Filesystem($child_twig_dir);
                self::$child_env = new Twig_Environment( $child_loader );
            } else {
                self::$child_env = false;
            }
        }

        if(!isset(self::$base_env)){
            $base_loader = new Twig_Loader_Filesystem($base_twig_dir);
            self::$base_env = new Twig_Environment( $base_loader );
        }

        // Load the child theme's template if it exists
        if(file_exists( $child_twig_dir . $filename )){
            return self::$child_env->loadTemplate( $filename );
        }

        // If not, load Samurai theme' tempalte file (this throws an exception if the file doesn' exist.)
        return self::$base_env->loadTemplate( $filename );

    }
}
