<?php

class PicolAdminQueryFilter{

    protected $display_name;
    protected $post_types;
    protected $meta_key;
    protected $possible_values;
    protected $name;

    static $current;

    function __construct( $display_name, $post_types, $meta_key, $possible_values ){

        $this->post_types = (array) $post_types;
        $this->meta_key = $meta_key;
        $this->display_name = $display_name;
        $this->possible_values = $possible_values;

        $this->name = $meta_key;

        add_action('restrict_manage_posts', array(&$this, '_onRestrictManagePosts'));
        add_filter('parse_query', array(&$this, '_onParseQuery'));
    }

    function _onRestrictManagePosts(){

        $type = _g('post_type', 'post');

        if (in_array($type, $this->post_types)) {

            $values     = $this->possible_values;
            $option_str = sprintf('<option value="">%s</option>', $this->display_name);

            foreach ( $values as $value => $label ) {

                $option_str .= sprintf(

                    '<option value="%1$s" %3$s>%2$s</option>',

                    /* 1 */ $value,
                    /* 2 */ $label,
                    /* 3 */ selected($value, _g($this->name), false)
                );

            }

            printf('<select name="%1$s">%2$s</select>', $this->name, $option_str);
        }
    }

    function _onParseQuery($query){
        global $pagenow;
        $type = _g('post_type', 'post');

        if ( in_array($type, $this->post_types) && is_admin() && $pagenow=='edit.php' && _g($this->name)) {
            $query->query_vars['meta_query'][] = array(
                'key' => $this->meta_key,
                'value' => _g($this->name)
            );
        }
    }
}