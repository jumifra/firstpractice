<?php
/**
 * Created by PhpStorm.
 * User: rodrigomurillo
 * Date: 11/30/14
 * Time: 7:12 PM
 */

defined('ABSPATH') or die("No script kiddies please!");

final class PicolTaxonomy{

    public $l_name;
    public $l_singular_name;
    public $l_search_items;
    public $l_all_items;
    public $l_parent_item;
    public $l_parent_item_colon;
    public $l_edit_item;
    public $l_update_item;
    public $l_add_new_item;
    public $l_new_item_name;
    public $l_menu_name;

    public $l_filter_showall;

    public $hierarchical = TRUE;
    public $show_ui = TRUE;
    public $show_admin_column = TRUE;
    public $query_var = TRUE;
    public $rewrite = array('slug' => 'new-taxonomy');

    protected $labels = FALSE;

    protected $tax_name;
    protected $slug;
    protected $post_types;
    protected $filter_post_types;

    function __construct( $tax_name, $slug, $post_types ) {

        $this->tax_name = $tax_name;
        $this->slug = $slug;
        $this->post_types = (array) $post_types;

        add_action('init', array(&$this, '_doRegisterTaxonomy'));
        add_action('restrict_manage_posts', array(&$this, '_restrictAddFilter'));

    }

    function autoLabels( $singular, $plural, $masculino = TRUE ) {

        $masculine_str = array(
            'search_items'      => _x('Search %s', 'masculine tax', 'pcs'),
            'all_items'         => _x('All %s', 'masculine tax', 'pcs'),
            'parent_item'       => _x('Parent %s', 'masculine tax', 'pcs'),
            'parent_item_colon' => _x('Parent %s:', 'masculine tax', 'pcs'),
            'edit_item'         => _x('Edit %s', 'masculine tax', 'pcs'),
            'update_item'       => _x('Update %s', 'masculine tax', 'pcs'),
            'add_new_item'      => _x('Add new %s', 'masculine tax', 'pcs'),
            'new_item_name'     => _x('New %s name', 'masculine tax', 'pcs'),
            'filter_showall'    => _x('Show all %s', 'masculine tax filter', 'pcs')
        );

        $feminine_str = array(
            'search_items'      => _x('Search %s', 'feminine tax', 'pcs'),
            'all_items'         => _x('All %s', 'feminine tax', 'pcs'),
            'parent_item'       => _x('Parent %s', 'feminine tax', 'pcs'),
            'parent_item_colon' => _x('Parent %s:', 'feminine tax', 'pcs'),
            'edit_item'         => _x('Edit %s', 'feminine tax', 'pcs'),
            'update_item'       => _x('Update %s', 'feminine tax', 'pcs'),
            'add_new_item'      => _x('Add new %s', 'feminine tax', 'pcs'),
            'new_item_name'     => _x('New %s name', 'feminine tax', 'pcs'),
            'filter_showall'    => _x('Show all %s', 'feminine tax filter', 'pcs')
        );

        $str_set = $masculino ? $masculine_str : $feminine_str;

        $this->l_name              = "$plural";
        $this->l_singular_name     = "$singular";
        $this->l_search_items      = sprintf($str_set['search_items'], $plural);
        $this->l_all_items         = sprintf($str_set['all_items'], $plural);
        $this->l_parent_item       = sprintf($str_set['parent_item'], $singular);
        $this->l_parent_item_colon = sprintf($str_set['parent_item_colon'], $singular);
        $this->l_edit_item         = sprintf($str_set['edit_item'], $singular);
        $this->l_update_item       = sprintf($str_set['update_item'], $singular);
        $this->l_add_new_item      = sprintf($str_set['add_new_item'], $singular);
        $this->l_new_item_name     = sprintf($str_set['new_item_name'], $singular);
        $this->l_menu_name         = "$plural";

        $this->l_filter_showall    = sprintf($str_set['filter_showall'], $plural);
    }

    function enableFilter( $post_types = array() ){

        $post_types = (array) $post_types;

        if(empty($post_types)){
            $post_types = $this->post_types;
        }

        $this->filter_post_types = $post_types;

    }

    function _restrictAddFilter(){

        global $typenow;

        if(!empty($this->filter_post_types)){
            if(in_array($typenow, $this->filter_post_types)){

                $tax_obj = get_taxonomy( $this->slug );

                $output = array();
                $terms = get_terms($tax_obj->name, array('hide_empty' => false));

                printf('<select name="%1$s">', $tax_obj->name);

                $output[] = sprintf('<option value="">%s</option>', $this->l_filter_showall);

                foreach($terms as $one_term) {
                    $selected = selected(_g($tax_obj->name), $one_term->slug, false);
                    $output[] = sprintf('<option %3$s value="%1$s">%2$s</option>', $one_term->slug, $one_term->name, $selected );
                }

                echo implode('', $output);

                echo '</select>';

            }
        }

    }

    function _doRegisterTaxonomy(){

        $this->rewrite = array( 'slug' => $this->slug );

        $tax_labels = array(
            'name'              => $this->l_name,
            'singular_name'     => $this->l_singular_name,
            'search_items'      => $this->l_search_items,
            'all_items'         => $this->l_all_items,
            'parent_item'       => $this->l_parent_item,
            'parent_item_colon' => $this->l_parent_item_colon,
            'edit_item'         => $this->l_edit_item,
            'update_item'       => $this->l_update_item,
            'add_new_item'      => $this->l_add_new_item,
            'new_item_name'     => $this->l_new_item_name,
            'menu_name'         => $this->l_menu_name
        );

        $tax_args = array(
            'labels'            => $tax_labels,
            'hierarchical'      => $this->hierarchical,
            'show_ui'           => $this->show_ui,
            'show_admin_column' => $this->show_admin_column,
            'query_var'         => $this->query_var,
            'rewrite'           => $this->rewrite
        );

        register_taxonomy( $this->tax_name, $this->post_types, $tax_args );

    }


}