<?php
/**
 * Created by PhpStorm.
 * User: rodrigomurillo
 * Date: 10/28/14
 * Time: 5:28 PM
 */

class PicolGitter {

    protected $log = array();
    protected $zip_file;
    protected $directory;

    function __construct($directory ){

        if(!is_dir( $directory )) {
            wp_die('PicolGitter: No pude leer el directorio '. $directory . ' para GIT');
        }

        $this->directory = $directory;


    }

    protected function log( $msj ){

        $msj = (array) $msj;

        foreach($msj as $un_msj){
            $this->log[] = $un_msj;
        }
    }

    function add( $file_string ) {
        chdir( $this->directory );

        $this->exec('git add "'.$file_string.'"');
    }

    function commit( $message ) {
        /*
         * Change Working Directory
         */

        chdir( $this->directory );

        $this->exec("git commit -a -m '$message' 2>&1");
        $this->log( exec('git branch 2>&1'));
    }


    function gitIt( $zip_file = FALSE ){

        $this->zip_file = $zip_file;

        /*
         * Change Working Directory
         */

        chdir( $this->directory );

        /*
         * Iniciar GIT
         */

        $this->gitInit();

        /*
         * HOOK post-receive
         */

        $this->configPostReceive();

        /*
         * Archivos iniciales de GIT
         */

        $this->createFiles();

    }

    function getLog(){

        $log = $this->log;
        $log_str = implode('<br />', $log);

        return $log_str;
    }

    protected function gitInit(){
        $git_user = Opciones::get( Opciones::GIT_BASE );
        $user_command = 'git config user.email "' . $git_user . '"';

        $this->exec('git init');

        $this->log( exec($user_command));
        $this->log( exec('git config user.name "Tu fiel servidor :)"'));

        $this->exec('git config receive.denyCurrentBranch ignore');
        $this->exec('git checkout master');
    }

    protected function exec($message){
        exec( $message, $lines );
        $this->log($lines);
    }

    protected function configPostReceive(){

        $git_dir = $this->directory;

        $post_hook = $git_dir . '/.git/hooks/post-receive';

        if( file_exists( $post_hook )) {
            unlink( $post_hook );
        }

        $file_h = fopen( $post_hook, 'w');

        fwrite( $file_h, 'GIT_WORK_TREE=../ git checkout -f');
        fclose( $file_h );

        $this->log( exec('chmod +x '.$git_dir.'/.git/hooks/post-receive'));

    }

    protected function createFiles(){

        if($this->zip_file){
            $zip = new ZipArchive();
            $zip->open( $this->zip_file );
            $zip->extractTo( $this->directory );
            $zip->close();
        } else {
            $readme = fopen( $this->directory . '/README.md', 'w');
            fwrite($readme, '# Git project');
            fclose($readme);
        }

        // Commit it!

        $this->log( exec('git add *.*'));
        $this->log( exec('git add *'));

        $this->log( exec('git commit -a -m "Initial commit" 2>&1'));

        $this->log( exec('git branch 2>&1'));
    }
}