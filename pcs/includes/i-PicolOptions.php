<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 2/16/2015
 * Time: 9:25 PM
 */

defined('ABSPATH') or die("No script kiddies please!");

class PicolOptions{

    const CAPABILITY_ADMIN = 'manage_options';
    const CAPABILITY_EDITOR = 'edit_others_posts';
    const CAPABILITY_USER = 'edit_posts';

    protected
        $menu_title,
        $html,
        $capability,
        $on_save,
        $page_title,
        $id,
        $parent,
        $icon,
        $fields,
        $scripts;

    function __construct($menu_title, $html, $capability = null, $on_save = null, $page_title = null, $id = null, $parent = null, $icon = null){
        $this->menu_title = $menu_title;
        $this->html = $html;
        if(empty($capability))
            $capability = self::CAPABILITY_EDITOR;
        $this->capability = $capability;
        $this->on_save = $on_save;
        if(empty($page_title))
            $page_title = $menu_title;
        $this->page_title = $page_title;

        if(empty($id))
            $id = sanitize_title($menu_title);
        $this->id =$id;

        $this->parent = $parent;
        $this->icon = $icon;

        if(!is_file($html))
            throw new Exception('No existe el archivo');

        add_action( 'admin_menu', array(&$this, 'createMenu') );
        //call register settings function
        add_action( 'admin_init', array(&$this, 'registerSettings') );

        add_action( 'admin_enqueue_scripts', array(&$this, 'registerScripts') );
    }

    public function createMenu() {
        //create new top-level menu
        add_menu_page($this->page_title, $this->menu_title, $this->capability, $this->id, array(&$this, 'printPage'), $this->icon);
    }

    function registerSettings() { // whitelist options
        if(is_array($this->fields)){
            foreach($this->fields as $field){
                register_setting( $this->id, $field );
            }
        }
    }

    public function registerField($name){
        $this->fields[] = $name;
    }

    public function registerScript($script){
        $this->scripts[] = $script;
    }

    public function printPage(){
        //pagina
        printf('<div class="wrap"><h2>%1$s</h2>', $this->page_title);
        echo '<form method="post" id="post" name="post">';
        settings_fields($this->id);
        do_settings_sections($this->id);
        include $this->html;
        echo '</form></div>';
    }


    public function registerScripts(){
        if($this->isCurrentPage()) {

            if (is_array($this->scripts)) {
                foreach ($this->scripts as $script) {
                    wp_enqueue_script('options_script', $script);
                }
            }

        }
    }

    protected function isCurrentPage(){
        global $pagenow;
        return $pagenow == 'admin.php' && _g('page') == $this->id;
    }

    function onPost( $callback ) {
        if($this->isCurrentPage() && doing_post_request()){
            call_user_func( $callback );
        }
    }

    function onThisPage( $callback ) {
        if($this->isCurrentPage()){
            call_user_func( $callback );
        }
    }

    function getUrl(){
        return admin_url('admin.php?page=' . $this->id );
    }

}