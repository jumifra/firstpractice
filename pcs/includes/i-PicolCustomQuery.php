<?php

class PicolCustomQuery {

  protected $query;
  protected $filtered;

  final function __construct( $args ){
    $this->query = new WP_Query( $args );
  }

  final function have_posts(){
    $doing_query_interception = false;

    if(!isset($this->filtered)){
      add_action('posts_clauses', array($this, '_intercept'), 20, 1);
    }

    $return = $this->query->have_posts();

    if($doing_query_interception){
      remove_action('posts_clauses', array($this, '_intercept'), 20);
    }

    return $return;
  }

  function fromFilter( $from_str ) {
    return $from_str;
  }

  function whereFilter ( $where_str ) {
    return $where_str;
  }

  function orderByFilter( $order_str ) {
    return $order_str;
  }

  function groupByFilter( $group_str ) {
    return $group_str;
  }

  function joinFilter( $join_str ) {
    return $join_str;
  }

}
