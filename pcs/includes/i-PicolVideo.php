<?php
/**
 * Created by PhpStorm.
 * User: rodrigomurillo
 * Date: 6/15/15
 * Time: 7:51 PM
 */

abstract class PicolVideo{

    protected $video_url;
    public $is_vimeo = false;
    public $is_youtube = false;


    abstract function getThumbUrl();
    abstract function getThumbWidth();
    abstract function getThumbHeight();
    abstract function getVideoHTML();

    function getID(){
        return false;
    }

    function getThumbData(){
        return array(
            $this->getThumbUrl(),
            $this->getThumbWidth(),
            $this->getThumbHeight()
        );
    }

    function __construct($vid_url) {
        $this->video_url = $vid_url;
    }


    /**
     * @param $video_url
     * @return PicolVideo | bool
     */

    static function getVideo( $video_url ){
        $class = self::getClassFromUrl( $video_url );

        if($class){
            $video = new $class($video_url);
            return $video;
        }

        return FALSE;

    }

    protected static function getClassFromUrl( $video_url ) {
        switch( TRUE ){
            case strpos($video_url, 'vimeo.com') !== FALSE:
                return 'PicolVimeoVideo';
            case strpos($video_url, 'youtube.com') !== FALSE:
            case strpos($video_url, 'youtu.be') !== FALSE:
                return 'PicolYoutubeVideo';
            default:
                return FALSE;
        }
    }

    protected function getHTMLSizeAttr($w = null, $h = null){
        $w_str = is_null($w) ? '' : 'width="'.$w.'"';
        $h_str = is_null($h) ? '' : 'height="'.$h.'"';
        return array( $w_str, $h_str );
    }
}