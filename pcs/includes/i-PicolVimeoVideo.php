<?php
/**
 * Created by PhpStorm.
 * User: rodrigomurillo
 * Date: 6/15/15
 * Time: 7:55 PM
 */

class PicolVimeoVideo extends PicolVideo{

    function __construct($vid_url) {
        $this->is_vimeo = true;
        parent::__construct( $vid_url );
    }

    function getID(){
        return $this->getVideoID();
    }

    function getVideoHTML(){
        $video_format = '//player.vimeo.com/video/%1$s';

        $id = $this->getVideoID( $this->video_url );
        if(!$id){
            return FALSE;
        }

        if (parse_url($id, PHP_URL_QUERY))
            $id .= '&title=0&amp;byline=0&amp;portrait=0';
        else
            $id .= '?title=0&amp;byline=0&amp;portrait=0';

        $dims = $this->getHTMLSizeAttr( null, null );
        $html_format = '<iframe class="embed-responsive-item" src="%1$s" %2$s %3$s frameborder="0"  allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"></iframe>';

        return sprintf($html_format, sprintf($video_format, $id), $dims[0], $dims[1]);
    }

    function getThumbUrl(){
        $id = $this->getVideoID();

        if( !$id ) {
            return FALSE;
        }

        $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$id.php"));

        return $hash[0]['thumbnail_large'];
    }

    function getThumbWidth(){
        return 640;
    }

    function getThumbHeight(){
        return 360;
    }

    protected function getVideoID(){
        $any_match = preg_match('/vimeo\.com\/([^\/]+)/', $this->video_url, $matches);

        if($any_match === FALSE) {
            return FALSE;
        }

        $id = $matches[1];

        return $id;
    }
}