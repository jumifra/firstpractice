var js = require('./gruntvars.js');
module.exports = function( grunt ) {
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        watch: {
            js: {
                files: ['src/js/**/*.js'],
                tasks: ['concat:all']
            },
            sass: {
                files: ['src/sass/**/*.scss'],
                tasks: ['sass:dist']
            }
        },
        sass: {
            options: {
                sourceMap: true,
                outputStyle: 'compressed'
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: 'src/sass',
                    src: ['*.scss'],
                    dest: '.',
                    ext: '.css'
                }]
            }
        },
        concat: {
            all: {
                src: [
                    'src/js/*.js'
                ],
                dest: 'concat.js'
            }
        },
        uglify: {
            all: {
                files: {
                    'concat.min.js': [
                        'concat.js'
                    ]
                }
            }
        }
        // Tasks
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-sass');

    // Default task(s).
    grunt.registerTask('default', ['concat:all', 'sass', 'watch']);
    grunt.registerTask('prod', ['concat:all', 'sass', 'uglify:all']);
    grunt.registerTask('styles', ['sass:dist']);

};